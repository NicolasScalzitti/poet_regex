#!/usr/bin/env python3

"""
Protein Optimization Engineering Tool Regular Expressions version (POET-Regex)
A Genetic Programming tool combined with regular expressions to generate protein sequences with a predictable function
Copyright (C) 2023  Scalzitti Nicolas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


@author: N. Scalzitti
@author: I. Alavy
@date: 09/23/2022
@department: Department of Engineering - Michigan State University
@version: 1.0
"""



# Librairies
import sys
sys.path.append('./Src') 

import time
import utils
import pprint
import os.path
import argparse
import optimizer
import multiprocessing
import random as R
import predictor as _predict
import evaluation as _eval
import individual as indi_module

from subprocess import call
from datetime import date
import readline


def call_header():
    """
    Poet-Regex header
    """

    blue = '\033[94m'
    utils.printcolor("\n #--------------------------------------------------------------#\n", blue)
    utils.printcolor('       ____   ___  _____ _____   ____                          ', blue)   
    utils.printcolor('      |  _ \ / _ \| ____|_   _| |  _ \ ___  __ _  _____  __    ', blue)
    utils.printcolor('      | |_) | | | |  _|   | |   | |_) / _ \/ _` |/ _ \ \/ /    ', blue)
    utils.printcolor('      |  __/| |_| | |___  | |   |  _ <  __/ (_| |  __/>  <     ', blue)
    utils.printcolor('      |_|    \___/|_____| |_|   |_| \_\___|\__, |\___/_/\_\    ', blue)
    utils.printcolor('                                           |___/               ', blue)
    utils.printcolor("\n #--------------------------------------------------------------#\n", blue)
    print('\nVersion 1.0')

def create_new_population(config, empty=False):
    """
    Generate a population of random individuals. The size of population
    is defined in the config.ini file. Return a list of individual, 
    except if empty is true. In this case, the population is empty

    config: The dictionnary with configuration parameters
    empty: a Boolean to define if the population contains individuals or not

    return: population
    """
    population = [] # list of individual
    pop_size = int(config["population_size"]) # size of the population
    init_method = str(config["init_pop_met"]) # Method to construct individual (grow, full or half)
    
    if empty:
        return population
    else:
        for _ in range(pop_size):
            indv = indi_module.Individual(config) # create a new individual
            indv.init_regex_pattern(init_method)
            population.append(indv)
    
        return population

def describe_pop(pop):
    '''
    display each individual in the population
    pop: the initial population
    '''

    for i, indi in enumerate(pop): 
        print('Individual :', i+1)
        indi.show()
     
def setup(config):
    """
    Ensure the appropriate directories exist. 
    Remove any existing log file them using appropriate time/date

    config: The dictionnary with configuration parameters
    """

    outputDir = "./output"
    os.makedirs(outputDir, exist_ok=True)       # Create the output dir
    utils.create_log_file(config["output_evo"])   # Create the evolutionary log file

def add_arguments_to_parser(parser):
    '''
    Construct the parser
    '''

    parser.add_argument('-v', '--verbose', help='Display some informative messages', action="store_true")
    parser.add_argument("-config", help="Takes the config file to configure the application (default: ./config.ini)", default="./config.ini")
    parser.add_argument('-r', '--run', help='Number of GP iterations to find a model', type=int)
    parser.add_argument('-p', '--population', help='Number of individual in the population', type=int)
    parser.add_argument('-t', '--tournament', help='Size of the tournament group for individual selection', type=int)
    parser.add_argument('-mr', '--max_rules', help='Max value of rules per individual', type=int)
    parser.add_argument('-mm', '--min_motif', help='Min size of detected motif', type=int)
    parser.add_argument('-tr', '--tree', help='Max depth of tree', type=int)
    parser.add_argument('-s', '--seed', help='The random seed', type=int)
    parser.add_argument('-o', '--output', help="Output directory")
    parser.add_argument('-f', '--file', help="training file path")

    parser.add_argument('-cpu', help='Number of CPU used by POET', type=int)
    parser.add_argument('-mo', help="Models directory - for hpc")
    parser.add_argument('-go', help="Graphs directory - for hpc")
    parser.add_argument('-lo', help="Logs directory - for hpc")

    parser.add_argument('-random', help="Random configuration, where weights are not adjusted", action="store_true")
    parser.add_argument('-hpc', help="Runs the experiment on the HPC servers through slurm jobs", action="store_true")
    parser.add_argument('-predict', help='Number of potential protein sequences you want the program to predict.', action="store_true")
    parser.add_argument('-evaluate', help='Evaluate a regex model on the training and test set', action="store_true")

    return parser

def manage_input(args):
    """
    Manage args before runs POET-Regex

    args: Args taken into account by the program

    return: the dictionar 'config' containing configuration information
    """

    # Config dictionary
    if args.config: 
        config = utils.read_config_file(args.config) # Create a dictionary from the config file
    else: 
        config = utils.read_config_file("config.ini") #  Create a dictionnary from a default config.ini file

    if args.verbose:
        config['verbose'] = args.verbose
    else:
        config['verbose'] = bool(False)
    if args.run: 
        config["runs"] = int(args.run)
    if args.population: 
        config["population_size"] = args.population
    if args.tournament: 
        config["tournament_size"] = int(args.tournament)
    if args.max_rules: 
        config["maximum_rule_count"] = int(args.max_rules)
    if args.min_motif:
        config["min_motif_size_db"] = int(args.min_motif)
    if args.tree:
        config["max_depth_tree"] = int(args.tree)
    if args.seed:
        config["seed"] = int(args.seed)
    else:
        R.seed(config["seed"]) # random seed
    
    if args.output: # Specifies the output directory
        try:
            if os.path.isdir(args.output):
                print(f"\033[91m[ERROR]\033[0m The output path: {args.output}, already exists")
                exit()
            else:
                os.makedirs(args.output, exist_ok=True)
                config["output_evo"] = args.output + '/evo.log' # evolutionnary info log
                config["output_model"] = args.output + '/model.csv'# model
                config["output_fig"] = args.output + '/Results/' # graphs
        except:
            print("\033[91m[ERROR]\033[0m Wrong output path")
            exit()
    if args.file:
        if os.path.exists(args.file):
            config['filename'] = args.file
        else: 
            print(f"\033[91m[ERROR]\033[0m The file {args.file} does not exist")
            exit() 
    if args.cpu:
        config["cpu"] = int(args.cpu)
    else:
        nb_cpu = multiprocessing.cpu_count()
        config["cpu"] = nb_cpu

    if args.mo: # Path of model directory for HPC
        config["output_model"] = args.mo
    if args.go: # Path of graph directory for HPC
        config["output_fig"] = args.go
    if args.lo: # Path of evo log directory for HPC
        config["output_evo"] = args.lo

    if args.random:
        config['random'] = args.random
    else:
        config['random'] = bool(False)

    if args.hpc: # Launch the hpc experiments
        hpc()
        exit(1)
    if args.predict: # Launch the prediction 
        run_prediction(config)
    if args.evaluate: # Evaluate a regex model
        run_evaluation(config)

    return config



def run_evaluation(config):
    '''
    Evaluate a regex model on a train and test datasets
    Depending of the information in the config.ini file

    config: the config file
    '''

    print("\033[32m[INFO]\033[0m Evaluation of a model:\n=========================================\n")
    config["output_model"] = input("Enter the path of the model to evaluate: ")
    print(f'\033[32m[INFO]\033[0m Dataset used: {config["filename"]}, with alphabet: {config["alphabet"]} ')
    objEval = _eval.Eval(config)

    training_set = f'./data/{config["filename"]}/train.csv'
    test_set = f'./data/{config["filename"]}/test.csv'

    df_train = objEval.eval_regex(training_set)
    df_test = objEval.eval_regex(test_set)
    
    objEval.plot_evaluation(df_train, df_test)
    exit()

def run_prediction(config):
    '''
    Run the directed evolution. Generate x new peptides and evaluate them with a regex model
    return the 20 best peptides
    '''

    print("\033[32m[INFO]\033[0m Predicting peptides:\n=========================================")
    filename   = input("Name of the output file: ")
    if os.path.exists('./predictions/'+filename+'.csv'):
        print(f"\033[91m[ERROR]\033[0m {filename}.csv already exists")
        exit()

    model_path = input("Enter the path of the model: ")
    if not os.path.exists(model_path):
        print(f"\033[91m[ERROR]\033[0m {model_path} is not found")
        exit()

    count      = int(input("Number of peptides to predict: "))
    seq_size   = int(input("Peptide size: "))

    print('\033[32m[INFO]\033[0m Larger values = more confident but similar predictions')
    print('\033[32m[INFO]\033[0m Lower values = less accurate predictions but makes room for novelty')
    iterations = int(input("Number of evolutionary iterations: "))
    
    print('\033[32m[INFO]\033[0m Other parameters in the config.ini file are take into account, such min motif size')
    predictor = _predict.Predictor(count, seq_size, iterations, config, model_path, filename)
    predictor.predict_one_model()
    exit()

def hpc():
    '''
    Run POET on the HPC
    '''

    today = date.today()
    # print(today)
    # day = today.strftime("%b_%d_%Y")
    day = today.strftime("%m_%d_%y")

    print("\033[32m[INFO]\033[0m POET Regex is running on the HPC experiment mode.\n\n"\
          "Please enter the following information\n"\
          "======================================\n")
    
    title   = input("Experiment Title: ")
    hours   = int(input("Requested Hours: "))
    nb_exp  = int(input("Number of Experiment Repeats: "))
    runs    = int(input("Evolutionary Generations: "))
    cpu     = int(input("Number of cpu: "))
    ram     = int(input("Number of ram: "))
    seed    = int(input("Starting Random Seed: "))
    config  = input("Experiment configuration file (empty for the default config.ini file): ")
    if config == '':
        config = 'config.ini'
    virtual_env = input("Virtual environnment name (empty for the current environment): ")
    if virtual_env == '':
        virtual_env = os.environ.get('CONDA_DEFAULT_ENV')
    project_path = os.getcwd()

    confirmation_text = f"\n======================================\n# Experiment Title: {title}\n# Hours: {hours}\n" \
                        f"# Repeats: {nb_exp}\n# Generations: {runs}\n# Seed: {seed}\n# Config: {config}\n# CPU: {cpu}\n# Virtual Envt: {virtual_env}\n# Project path: {project_path}"

    print(confirmation_text)
    title = day + "_" + title

    if os.path.exists(f"output/{title}"):
        print("\033[91m[ERROR]\033[0m An experiment with the same title exists")
        exit()
    else:
        directory         = os.path.join("output", title) # Main dir of the experiment
        subs_directory    = os.path.join(directory, "subs")
        slurms_directory  = os.path.join(directory, "slurms")
        results_directory = os.path.join(directory, "Results")
        errors_directory  = os.path.join(directory, "errors")
        logs_directory    = os.path.join(results_directory, "logs")
        models_directory  = os.path.join(results_directory, "models")
        fig_directory     = os.path.join(results_directory, "figures")

        dirs = [directory, subs_directory, logs_directory, models_directory,fig_directory, errors_directory, slurms_directory]
        
        for d in dirs:
            os.makedirs(d)

        print("\033[32m[INFO]\033[0m Initialization of directories")

    content = confirmation_text

    # Config parameters
    try:
        with open(config, "r") as cfile:
            content += "\n" + cfile.read()
    except FileNotFoundError:
        print(f"\033[91m[ERROR]\033[0m The file {config} does not exist. Please select a good file")
        exit()

    # Create the file config.ini
    utils.writeTruncate(os.path.join(directory, "config.ini"), content)

    print(f'\033[32m[INFO]\033[0m Config file path: {os.path.abspath(directory + "/config.ini")}')

    for i in range(nb_exp): # Number of exp.

        filename = os.path.join(subs_directory, f"{title}_{i}.sb")

        batchFile = open(filename, "w")

        batchFile.write("#!/bin/bash --login\n")
        batchFile.write("\n########## SBATCH Lines for Resource Request ##########\n\n")
        batchFile.write(f"#SBATCH --time={hours}:02:00        # limit of wall clock time - how long the job will run (same as -t)\n")
        batchFile.write( "#SBATCH --nodes=1                   # number of different nodes - could be an exact number or a range of nodes (same as -N)\n")
        batchFile.write( "#SBATCH --ntasks=1                  # number of tasks - how many tasks (nodes) that you require (same as -n)\n")
        batchFile.write(f"#SBATCH --cpus-per-task={cpu}       # number of CPUs (or cores) per task (same as -c)\n")
        batchFile.write(f"#SBATCH --mem-per-cpu={ram}G           # memory required per allocated CPU (or core) - amount of memory (in bytes)\n")
        batchFile.write(f"#SBATCH --job-name {title}_{i}      # you can give your job a name for easier identification (same as -J)\n")
        batchFile.write(f"#SBATCH --error={errors_directory}/{title}_{i}.err       # Error path\n")
        batchFile.write(f"#SBATCH --output={slurms_directory}/{title}_{i}.txt      # Output path\n")

        # SBATCH --error=%j.err
        batchFile.write("\n########## Command Lines to Run ##########\n\n")
        # batchFile.write("module pandas\n")

        batchFile.write(f"conda activate {virtual_env}\n") # Load the virtual environment
        batchFile.write(f"cd {project_path}\n")

        log_file_path   = logs_directory + f"/evo_{i}.csv"
        model_file_path = models_directory + f"/model_{i}.csv"
        graph_file_path = fig_directory + f"/fig_{i}/"
        config_path = directory + '/config.ini'

        batchFile.write('ulimit -c 0 \n') # avoid core dumps
        batchFile.write(f"srun -n 1 python poet.py -config {config_path} -r {runs} -lo {log_file_path} -mo {model_file_path} -go {graph_file_path} --seed {i + seed}\n")
        batchFile.write("scontrol show job $SLURM_JOB_ID     ### write job information to output file")
        batchFile.close()

        # Run on HPC
        call(["sbatch", os.path.join(subs_directory, f"{title}_{i}.sb")])




def run_evolution():
    """
    Main funcion to run POET-Regex and train a model
    """

    call_header() # graphical logo

    ###########################################
    ###                                     ###
    ###           CONFIGURATION             ###
    ###                                     ###
    ###########################################


    # Argument descriptions
    parser = argparse.ArgumentParser(
        description='\033[32m[DESCRIPTION]\033[0m Protein Optimization Engineernig Tool with Regular Expressions (POET-Regex) is a protein '\
                    'function prediction program based on a genetic programming algorithm combined with regular expressions.'\
                    'POET-regex models learn the inherent characteristics of proteins from a training dataset.'\
                    'The models are then able to predict the fitness value of the new generated proteins.')

    parser = add_arguments_to_parser(parser) # Building the parser to take args into account
    args = parser.parse_args()
    config = manage_input(args) # Generate the config dictionary options
    setup(config)


    ###########################################
    ###                                     ###
    ###             EVOLUTION               ###
    ###                                     ###
    ###########################################


    # Initialization step

    print(f"\033[32m[INFO]\033[0m Initialization of a population of size {config['population_size']} "\
          f"using the {config['init_pop_met'].upper()} method ")

    # Create a random population
    start_pop_time = time.time()
    initial_population = create_new_population(config)
    pop_time = round(time.time() - start_pop_time, 3)

    # describe_pop(initial_population)

    if config['verbose']:
        print(f"\033[32m[INFO]\033[0m Creation of the population in {pop_time} s.")
        print("\033[32m[INFO]\033[0m Config file:")
        pprint.pprint(config)

    # Core of the evolutionary algorithm
    optimizer.evolve(config, initial_population)


if __name__ == "__main__":

    run_evolution()
