<div align="center">
<img src="./data/POET_logo.png" align="center" width="300" height="180"> 
<p>Protein Optimization Engineernig Tool with Regular Expressions (POET-Regex)</p>
</div>



![release](https://img.shields.io/badge/Version-1.0-blue.svg)

## ℹ Informations

#### Description

__POET-Regex__ is a protein engineering program to predict peptides with a specific functions. It is based on a genetic programming algorithm combined with regular expressions. The strength of __POET-Regex__ is based on the evolutionary process of finding original solutions, coupled with the power and flexibility of regular expressions to identify specific motifs. __POET-Regex__ can be used to generate peptides for a wide range of applications.

## 🛠 Installation

#### Create the project:

``` bash
git clone https://gitlab.com/NicolasScalzitti/poet_regex.git
```

#### Go to the project directory

```bash
cd poet_regex
```

#### Installing the virtual environment

For better performance, use the provided environment.
```bash
conda env create -f requirements.yml
conda activate ENV_NAME
```

#### Virtual environment activation

```bash
conda activate ENV_NAME
```

*ENV_NAME* is the name of the virtual environment created. To change the ENV_NAME, modify line 1 (name:) in *requirements.yml*. (Default = __regex_env__)


## ▶ Run POET Regex to train a model

To see the help:
``` bash
python ./poet.py -h
```

Basic launch with parameters from the file *./config.ini*:
``` bash
python ./poet.py
```

Activate Verbose mode (Show more information)
``` bash
python ./poet.py -v
```

Modification of the size of the population (-p) and/or the number of generations (-r)
``` bash
python ./poet.py -p 1000
python ./poet.py -r 100
python ./poet.py -p 1000 -r 100
```

Run __POET-Regex__ without weight adjustment based on the motif database generated with the data
``` bash
python ./poet.py -random
```

Save results in output directory:
```bash
python ./poet.py -o my_output_dir
```

Choose the number of CPU to use (the more cpu, the faster the evaluation step).
```bash
python ./poet.py -cpu 10
```

[![asciicast](https://asciinema.org/a/591045.svg)](https://asciinema.org/a/591045)

## ▶ Run POET Regex with Slurm on HPC

```bash
python ./poet.py -hpc
```

💻 __On the terminal__:  
Experiment Title: *ENTER THE NAME OF YOUR PROJECT*  
Requested Hours: *ENTER THE NUMBER OF HOURS TO ALLOCATE*  
Number of Experiment Repeats: *ENTER NUMBER OF EXPERIMENTS (number of jobs)*  
Evolutionary Generations: *ENTER NUMBER OF GENERATIONS*  
Number of cpu: *ENTER THE NUMBER OF CPU TO ALLOCATE*  
Number of ram: *ENTER NUMBER OF RAM TO ALLOCATE (select >10GB)*  
Starting Random Seed: *ENTER A SEED (int)*  
Experiment configuration file: *ENTER THE PATH TO A CONFIG.INI FILE, IF EMPTY, IT USES THE DEFAULT config.ini FILE*  
Virtual environnment name (empty for the current environment): *ENTER THE PATH TO A VIRTUAL ENVIRONMENT (IF EMPTY, IT USES THE CURRENT ENVIRONMENT)*  

See your jobs :
``` bash
qstat -u USERNAME
  # or
squeue -u USERNAME
```

Cancel a job:
``` bash
scancel Job_ID
```
Results (default path) are in ./poet_regex/output/{date}\_PROJECT_NAME

See the best and average results of all hpc experiments:
``` bash
cd ./Src
python ./analyze_hpc.py PATH_OF_PROJECT
  # example:
python ./analyze_hpc.py ../output/{date}_PROJECT_NAME
```



## Generate peptides with a POET Regex model

Run prediction
``` bash
python ./poet.py -predict

```

💻 __On the terminal__:  
Name of the output file: *ENTER A PATH FOR THE RESULT FILE (without extension)*  
Enter the path of the model: *ENTER THE PATH OF YOUR MODEL*  
Number of proteins to predict: *ENTER THE FINAL NUMBER OF PEPTIDES YOU REQUIRE*  
Protein size: *ENTER THE SIZE OF THE PREDICTED PEPTIDES*  
Number of evolutionary iterations: *ENTER THE NUMBER OF GENERATIONS*

Predictions are in ./poet_regex/predictions/OUTPUT_FILE.csv

⚠ *A filter is added to select only hydrophilic peptides. To activate or deactivate the filter, comment lines 91-110 and uncomment lines 114-127 (specify in script).*


## Evaluate a POET Regex model

Run evaluation
``` bash
python ./poet.py -evaluate
```

💻 __On the terminal__:  
Enter the path of the model to evaluate: *ENTER THE PATH OF YOUR MODEL*  
ex: *./example.csv*  



## Usage
pyhton poet.py [-h] [-v] [-config CONFIG] [-r RUN] [-p POPULATION] [-t TOURNAMENT] [-mr MAX_RULES] [-mm MIN_MOTIF] [-tr TREE] [-s SEED] [-o OUTPUT] [-f FILE] [-cpu CPU] [-mo MO] [-go GO] [-lo LO] [-hpc] [-predict] [-evaluate]

| Parameters|              | Description                                                           |
|-----------|--------------|-----------------------------------------------------------------------|
| -h        | --help       | Show this help message and exit                                       |
| -v        | --verbose    | Display some informative messages                                     |
| -config   |              | The config file to configure the application (default: ./config.ini)  |
| -r        | --run        | Number of GP iterations to evolve a model                             |
| -p        | --population | Number of individual in the population                                |
| -t        | --tournament | Size of the tournament group for individual selection                 |
| -mr       | --max_rules  | Max value of rules per individual                                     |
| -mm       | --min_motif  | Min size of detected motif                                            |
| -tr       | --tree       | Max depth of tree                                                     |
| -s        | --seed       | The random seed                                                       |
| -o        | --output     | Output directory                                                      |
| -f        | --file       | Training file path                                                    |
| -cpu      |              | Number of CPU used by POET                                            |
| -mo       |              | Models directory - for hpc                                            |
| -go       |              | Graphs directory - for hpc                                            |
| -lo       |              | Logs directory - for hpc                                              |
| -hpc      |              | Runs the experiment on the HPC servers through slurm jobs             |
| -predict  |              | Number of potential protein sequences you want the program to predict |
| -evaluate |              | Evaluate a regex model on the training and test set                   |
| -random   |              | Random configuration, where weights are not adjusted                  |
 

## 🔧 Config.ini file

The *./config.ini* file contains the essential information for the proper functionning of the __POET-Regex__ program:

#### General
__filename__ = name of data directory (str)  
__alphabet__ = name of the alphabet (classical=20 classical amino acids) (str)  
__seed__ = a value for the seed (int) 
__population_size__ = size of the population (int)  
__runs__ = number of generations of the evolutionary process (int)  
__maximum_rule_count__ = max number of regex per individual (int)  

#### Database
__db_strat__ = strategy to build the Motif Database (default=occ)  
__max_motif_size_db__ = max value of the motif size (int)  
__min_motif_size_db__ = min value of the motif size (int)  
__threshold__ = value that defines the group of a motif (1=motif is important (> threshold) and 0=motif is not favorable (< threshold)) (float).  

#### Output
__output_evo__ = path of the evolutionary log (str)  
__output_model__ = path of the models (str)   
__output_fig__ = path of the results directory (str)  

#### Crossover
__proba_xover__ = probability of an individual making a crossover (float)    
__tournament_size__ = size of the tournament in the selection method (int)  

#### Mutations
__mut_add_rule__ = probability of the mutation add_rule (float)   
__mut_remove_rule__ = probability of the mutation remove_rule (float)   
__mut_replace_rule__ = probability of the mutation replace_rule (float)   
__mut_replace_subtree__ = probability of the mutation replace_subtree (float)   
__mut_change_weight__ = probability of the mutation change_weight (float)   
__mut_remove_from_pattern__ = probability of the mutation remove_from_pattern (remove subtree) (float)   
__mut_point__ = probability of the mutation point (float)   
__mut_add_aa__ = probability of the mutation add_aa (float)   

#### Individuals
__max_depth_tree__ = individual tree depth (int)  
__min_braces__ = min value between {} (int)  
__max_braces__ = max value between {} (int)  
__init_pop_met__ = strategy to build individual (half, grow or full)  
__insertion_pop__ = method to add individual in the final population ('mix'=add offspring in the previous generation to mix parent + offspring or 'generational'=replace the population with only offspring)  

#### Others
__simil_threshold__ = minimum percentage to determine whether 2 regex are similar (float)   
__verbose__ = display or not some information during the evolutionary process (bool)  
__stop__ = interrupt the evolution process if the fitness of the best individual doesn't change anymore during {stop} run (int)  



## ➡ Add your data

This section is for training a __POET-Regex__ model with your own data.

You need a raw file *YOUR_RAW_FILE.csv*, with these 2 columns: __'sequence'__ and __'fitness'__. 
The __'sequence'__ column contains the peptide sequences and the __'fitness'__ column contains the associated fitness value or experimental value.

Move the *YOUR_RAW_FILE.csv* file to RAW directory (in data directory).
``` bash
cp YOUR_RAW_FILE.csv ./poet_regex/data/RAW
```

Next, generate a training and test sets. Use the *./data_converter.py* script.  

``` bash
cd ./Src
python ./data_converter.py
```


💻 __On the terminal__:  
Name of raw data file: YOUR_RAW_FILE (without extension)  
Name of the alphabet (Empty = classical): USE classical TO USE THE 20 CLASSICAL AMINO ACIDS

The script creates a directory in 'data' with the same name as *YOUR_RAW_FILE* file in RAW, where the files train.csv and test.csv are located.  

then modify the line in the *config.ini* file:  
#### General  
__filename__ = YOUR_RAW_FILE  



## Support
✉ scalzit1@msu.edu or n.scalzitti@yahoo.com  
✉ banzhafw@msu.edu

## License


