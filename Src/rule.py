#!/usr/bin/env python3

"""
Class Rule. Create the rules/Regex for the individual

@author: I. Alavy
@author: N. Scalzitti
@date: 09/23/2022
@department: Department of Engineering - Michigan State University
"""


class Rule:
	def __init__(self, pattern, tree, weight):
		self.pattern = pattern # The pattern of the RE, readable by human
		self.weight = weight	# a weight to identify the rule
		self.tree_shape = tree # The shape in a list format of the RE
		self.score = weight			# score of the RE, adjusted with the training stp
		self.db_motif = {}		# list of motifs identified by the RE
		self.nb_match = 0		# number of matches


	