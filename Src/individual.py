#!/usr/bin/env python3

"""
Class Individual. Create different individuals for the population

@author: I. Alavy
@author: N. Scalzitti
@date: 09/23/2022
@department: Department of Engineering - Michigan State University
"""

import re
import copy
import random
import regex_lib
import random as R
import rule as Rule
import pandas as pd
from fuzzywuzzy import fuzz


class Individual:

	start_ID = 1
	 
	def __init__(self, config):
		self.config = config
		self.rules = [] 												# List of all rules/regex
		self.maxRuleCount = int(config["maximum_rule_count"]) 			# Max number of regex per individual
		self.minWeight = -50.0											# Min identification number
		self.maxWeight = 50.0 											# Max identification number
		self.fitness = 0 												# Fitness of the individual
		self.depth_tree = int(config["max_depth_tree"])					# Max deepth of the tree shape
		self.min_braces = int(config["min_braces"])						# Min value in braces
		self.max_braces = int(config["max_braces"])						# Max value in braces
		self.similarity_threshold = float(config['simil_threshold']) 	# Max similarity threshold between 2 rules
		self.id = Individual.start_ID									# ID of the individual
		self.train_score = 0 											# Score obtained during the train step

		Individual.start_ID+=1 											# ID counter

	
	def reset(self):
		'''
		Reset values of an individual:
		    - score, db_motif, nb_match
		'''

		self.fitness = 0 # set the fitness

		# Make sure that every rule status is 0
		for rule in self.rules:
			rule.score = 0
			rule.db_motif = {}
			rule.nb_match = 0

	def check_pattern(self, pattern):
		'''
		Check if the regex pattern compiles well

		pattern: pattern of the regex
		'''

		if pattern == None:
			return False
		else: 
			try:
				re.compile(pattern)
				return True
			except:
				return False

	def init_regex_pattern(self, init_method):
		'''
		Generate the list of rules/regex of an individual

		init_method: method to build the tree shape of the individual (grow, full or half)
		'''

		# Create rules/regex between 1 and (maxRuleCount/4)
		for _ in range(R.randint(1, int(self.maxRuleCount/4) )):

			# Create a random weight = identifier
			# weight = round(R.uniform(self.minWeight, self.maxWeight), 2)

			if self.config['random']:
				score = round(R.uniform(self.minWeight, self.maxWeight), 2)
			else:
				score = 0

			# Create a new regex pattern according to a tree construction method
			if init_method == 'half':
				pattern_re, tree = regex_lib.indi_half(self.depth_tree, self.min_braces, self.max_braces, self.config)
			elif init_method == 'grow':
				pattern_re, tree = regex_lib.indi_grow(self.depth_tree, self.min_braces, self.max_braces, self.config)
			elif init_method == 'full':
				pattern_re, tree = regex_lib.indi_full(self.depth_tree, self.min_braces, self.max_braces, self.config)
			else:
				print('\033[91m[ERROR]\033[0m Initialization method Error, choose between: grow, full or half in config.ini file')
				exit()

			# checkpoint_1
			if self.check_pattern(pattern_re):
				rule = Rule.Rule(pattern_re, tree, score)
				rule.compiling_pattern = re.compile(pattern_re)

				# checkpoint_2 - the root must be a 'cat' or '|' node
				if rule.tree_shape[0] != 'cat' and rule.tree_shape[0] != '|':
					print('\033[91m[ERROR]\033[0m Root is incorrect:', rule.tree_shape)
					exit()

				# Add the new regex to the list of rules of the individual
				self.rules.append(rule)
			if len(self.rules) == 0:
				print('[ERROR] No rules in the individual', self.ID)
				exit()

			# self.scoreSort()

	def scoreSort(self):
		'''
		Sort the list of Rules according to the score of the rule. 
		The first element has the highest score and the last the smallest.
		'''
		self.rules = sorted(self.rules, key=lambda x: x.score, reverse=True)
		
	def remove_duplicate_rules(self):
		'''			
		Remove identical rules
		'''

		set_pattern = set()
		final = []

		# Supprime les regles qvec un score = 0.0
		# for rule in self.rules:
		# 	if rule.score == 0.0:
		# 		self.rules.remove(rule)

		for rule in self.rules:
			if rule.pattern not in set_pattern:
				set_pattern.add(rule.pattern)
				final.append(rule)

		self.rules.clear()  # Reset the list of rules
		self.rules = copy.copy(final) # replaces the list of rules by a list without duplicates
		
	def select_double_rules(self):
		'''
		Generate a list of tuple with similar regex
		if similarity > similarity_threshold (default=52)
		The similarity computing is based on the Levensthein distance
		'''


		set_simil_rules = set()

		for i, rule1 in enumerate(self.rules):
			for j, rule2 in enumerate(self.rules):
				if i == j:
					pass
				else:
					if rule1.weight == rule2.weight:
						similarity  = fuzz.ratio(rule1.pattern, rule2.pattern)

						if similarity  >= self.similarity_threshold:
							set_simil_rules.add(tuple(sorted((i, j))))
					
					elif rule1.score == rule2.score:
						similarity  = fuzz.ratio(rule1.pattern, rule2.pattern)

						if similarity  >= self.similarity_threshold:
							set_simil_rules.add(tuple(sorted((i, j))))

		return set_simil_rules

	def remove_double_rules(self):
		'''
		Remove identical rules
			and
		Remove rule with high similiraty according to the threshold: self.similarity_threshold
		'''

		# Step 1 - Remove duplicates
		self.remove_duplicate_rules()

		# Step 2 - Remove similar rules
		set_simil_rules = self.select_double_rules()
		set_rem = set()

		for t in set_simil_rules:
			if self.rules[t[0]].score >= self.rules[t[1]].score:
				set_rem.add(self.rules[t[1]])
			else:
				set_rem.add(self.rules[t[0]])

		# Delete a similar rule with lowest score
		for r in set_rem:
			self.rules.remove(r)
			
	def show(self):
		'''
		Print the pattern and the score of each rule of the individual
		'''

		for i, rule in enumerate(self.rules):
			print(f"#{i+1} {rule.pattern} - {round(rule.score,2)} - {rule.nb_match} - #{self.id}")
		print(f"Fitness = {round(self.fitness,3)}\n")
