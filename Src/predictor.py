#!/usr/bin/env python3

"""
Predict the fitness value for new peptides with a POET-regex model

@author: I. Alavy
@author: N. Scalzitti
@date: 02/20/2023
@department: Department of Engineering - Michigan State University
"""

import os
import re
import copy
import random as R
import pandas as pd
from tqdm import tqdm


class Sequence:
    def __init__(self, pattern, fitness):
        self.pattern = pattern
        self.fitness = fitness
        
class Predictor:

    def __init__(self, count, size, iterations, config, model_path, filename):
        self.config = config                    # config file
        self.model_path = model_path            # pqath of the regex model
        self.count = count                      # Number of protein to predict
        self.size = size                        # Size of new proteins
        self.iterations = iterations            # evolutionary iterations
        self.list_new_peptides = []             # list of new peptides
        self.codes = self.convert_alphabet_to_AA('data/ALPHABET/' + config['alphabet'] + '.csv') # AA list
        self.min_size_motif = int(config['min_motif_size_db'])                 # Minimal size of the motif that the regex model can detect
        self.filename = filename
        self.outputDir = f"./predictions"

        self.hydroDic = {
            "I": -0.31,
            "L": -0.56,
            "F": -1.13,
            "V": 0.07,
            "M": -0.23,
            "P": 0.45,
            "W": -1.85,
            "T": 0.14,
            "E": 2.02,
            "Q": 0.58,
            "C": -0.24,
            "Y": -0.94,
            "A": 0.17,
            "S": 0.13,
            "N": 0.42,
            "D": 1.23,
            "R": 0.81,
            "G": 0.01,
            "H": 0.96,
            "K": 0.99
        } 


    def generate_new_peptide(self):
        '''
        Generate a random peptide
        return a peptide sequence
        '''

        new_peptide = ''
        for j in range(self.size):
            rand = R.randint(0, len(self.codes) - 1)
            new_peptide += self.codes[rand]

        return new_peptide

    def evaluate(self, model, new_peptide):
        '''
        Evaluation of a new peptide with a regex model
        model: a regex model
        new_peptide: a new peptide to evaluate

        return the predicted fitness
        '''

        os.makedirs(self.outputDir, exist_ok=True)


        # COMMENT TO USE THE PREDICTION WITHOUT THE HYDROPHOBIC FILTER

        # Check hydrophobicity
        hydrophobicity = self.hydrophobic(new_peptide)

        if hydrophobicity >= 0:
            predicted_fitness = 0
            dictionary = model.set_index('pattern')['weight'].to_dict()

            for regex, score in dictionary.items():
                regex = re.compile(regex)
                object_find = re.finditer(regex, new_peptide)

                for match in object_find:
                    motif = match.group()

                    if len(motif) >= self.min_size_motif:
                        predicted_fitness += score

            return predicted_fitness
        else:
            return 0

        # UNCOMMENT TO USE THE PREDICTION WITHOUT THE HYDROPHOBIC FILTER

        # predicted_fitness = 0
        # dictionary = model.set_index('pattern')['weight'].to_dict()

        # for regex, score in dictionary.items():
        #     regex = re.compile(regex)
        #     object_find = re.finditer(regex, new_peptide)

        #     for match in object_find:
        #         motif = match.group()

        #         if len(motif) >= self.min_size_motif:
        #             predicted_fitness += score

        # return predicted_fitness

        # 

    def initial_pop(self, model):
        '''
        Generate a random population of new peptides
        Evaluate each new peptide with a regex model

        model: a regex model
        '''

        for i in range(self.count):
            new_peptide = ""

            # Peptide size
            if self.size <= 0:
                self.size = R.randint(8, 12)

            # Generate peptide sequence
            new_peptide = self.generate_new_peptide()
            # Evaluate peptide
            predicted_fitness = self.evaluate(model, new_peptide)

            sequence = Sequence(new_peptide, predicted_fitness)
            self.list_new_peptides.append(sequence)

    def hydrophobic(self, pattern):
        '''
        Compute the hydrophobicity of a AA sequence
        pattern: a peptide sequence

        return True if the pattern is hydrophobic and return False is the pattern is hydrophilic
        '''

        hydro_value = 0
        for char in pattern:
            hydro_value += self.hydroDic[char]

        return hydro_value

    def sort(self):
        '''
        Sort new peptide sequence according to their fitness function
        '''

        n = len(self.list_new_peptides)
        # Traverse through all array elements
        for i in range(n):
            # Last i elements are already in place
            for j in range(0, n - i - 1):
                # traverse the array from 0 to n-i-1
                # Swap if the element found is greater
                # than the next element
                if self.list_new_peptides[j].fitness < self.list_new_peptides[j + 1].fitness:
                    self.list_new_peptides[j], self.list_new_peptides[j + 1] = self.list_new_peptides[j + 1], self.list_new_peptides[j]

    def convert_alphabet_to_AA(self, path):
        '''
        Extracts only the AA from the 'learn_data' file
        path: path of the 'learn_data' file

        return a list of AA
        '''

        df = pd.read_csv(path, sep=';', header=None)
        df.columns = ['AminoAcid', 'Arity']
        df_AA = df.loc[df['Arity'] == 0]

        return list(df_AA['AminoAcid'])

    def predict_one_model(self):


        # Load the model
        model = pd.read_csv(self.model_path)

        print("\033[32m[INFO]\033[0m Generate random new peptides")
        # Generate peptides
        self.initial_pop(model)

        # Load training data
        learn_data = pd.read_csv(f'data/{self.config["filename"]}/train.csv')
        training_set = learn_data["sequence"].tolist()
        
        # Displays the sorted peptides
        if self.config['verbose']:
            self.sort()
            for i, pept in enumerate(self.list_new_peptides):
                if i < 20:
                    print(f'{i+1}:\t {pept.pattern}\t{round(pept.fitness,2)}')

        print("\033[32m[INFO]\033[0m Run Directed Evolution")
        
        for i in tqdm(range(self.iterations)):

            for myi, peptide in enumerate(self.list_new_peptides):
                randomSite = R.randrange(self.size)
                newAA = self.codes[R.randrange(len(self.codes))]
                newPeptide = copy.copy(peptide)

                newPeptide.pattern = newPeptide.pattern[:randomSite] + newAA + newPeptide.pattern[randomSite + 1:]

                if newPeptide.pattern in self.list_new_peptides:
                    newPeptide.fitness = 0
                elif newPeptide.pattern in training_set:
                    print("\n already in the dataset \n")
                    newPeptide.fitness = 0
                else:
                    newPeptide.fitness = self.evaluate(model, newPeptide.pattern)
    
                if newPeptide.fitness > peptide.fitness:
                    self.list_new_peptides[myi] = newPeptide

        # Sorts new peptides according to their predicted fitness
        self.sort()
        print("\n=========================================")
        data_20 = []
        data = []

        for i, pept in enumerate(self.list_new_peptides):
            if i < 20:
                hydrophobicity = self.hydrophobic(pept.pattern)
                data_20.append([i+1, pept.pattern, round(hydrophobicity,3), round(pept.fitness, 2)])
            data.append([i+1, pept.pattern, round(hydrophobicity,3), round(pept.fitness, 2)])


        df20 = pd.DataFrame(data_20, columns=['Index', 'Predicted_peptides', 'hydrophobicity', 'Fitness'])
        df = pd.DataFrame(data, columns=['Index', 'Predicted_peptides', 'hydrophobicity', 'Fitness'])
        print(df20.to_string(index=False))

        df.to_csv(f'{self.outputDir}/{self.filename}.csv', index=False)






