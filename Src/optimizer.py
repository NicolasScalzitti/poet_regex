#!/usr/bin/env python3

"""
Core of the evolutionary process based on GP algorithm combined with regular expressions

@author: N. Scalzitti
@author: I. Alavy
@date: 10/12/2022
@department: Department of Engineering - Michigan State University
"""

import utils
import regex_lib
import rule as Rule
import individual as I
import evaluation as Evaluation

import re
import os
import sys
import copy
import time
import math
import warnings
import threading
import random as R
import numpy as np
import scipy as sp
import pandas as pd
import seaborn as sns
import multiprocessing
import matplotlib.pyplot as plt
from tqdm import tqdm
from scipy import stats
from multiprocessing import Pool
from statistics import mean, stdev
from sklearn.model_selection import KFold

MAX = 0
loading = False

def build_motif_database(config):
	'''
	Build the training database with all motifs in the training set
	Depends on the strategy:
	- std: the best value of the same motif defines the class
	- avg: the average of fitness values of the same motifs is calculated,
	       and if it is above the threshold, then class 1 is defined for this motif
	- occ: The number of patterns in each class defines the class (BEST)

	config: the dictionnary with configuration parameters

	return: training_db_motif
	'''

	global MAX
	db_strat = config['db_strat']

	for i in range (int(config['min_motif_size_db']), int(config['max_motif_size_db'])+1):
		# Strategies
		if db_strat == 'std':  # Best value
			training_db_motif = utils.standard_motif_bank(i, config['filename'])
		if db_strat == 'avg':  # Average value 
			training_db_motif = utils.average_motif_bank(i, config['filename'])
		if db_strat == 'occ':  # Number of motif in each class define the classe
			training_db_motif = utils.occ_motif_bank(i, config['filename'], float(config['threshold']))



	# extract the highest Value
	m_set = set()
	for k,v in training_db_motif.items():
		m_set.add(v[1])
	MAX = max(m_set) 

	return training_db_motif

def load_training_file(config):
	'''
	Load the training dataset and create a df

	config: the dictionnary with configuration parameters

	return: training_file, a df containing the training data
	'''

	try:
		training_file = pd.read_csv(f'data/{config["filename"]}/train.csv', sep=',')
		print(f'\033[32m[INFO]\033[0m Training data used: {config["filename"]}')
		print(f'\033[32m[INFO]\033[0m Alphabet used: {config["alphabet"]}')
	except AttributeError:
		print("\033[91m[ERROR]\033[0m 'learn_data' in config.ini is not set (check if separator is , )")
		exit()

	return training_file

def stop_condition(stop_fitness, stop=5):
	"""
		Interrupt the evolution process if the fitness of the best individual 
		doesn't change anymore during {stop} runs.

		stop_fitness: list with fitness values for each run
		stop: Number of runs before the process stops		

		retur: boolean
	"""

	if len(stop_fitness) < stop:
		return False

	for i in range(len(stop_fitness) - stop, len(stop_fitness)):
		if stop_fitness[i] != stop_fitness[i-1]:
			return False

	return True

def loading_animation():
	'''
	Loading bar
	'''
	global loading
	animation = "|/-\\"
	i = 0

	while not loading:
		sys.stdout.write('\r' + "\033[32m[INFO]\033[0m Loading results and preparing figures " + animation[i % len(animation)])
		sys.stdout.flush()
		time.sleep(0.1)
		i += 1

def generate_train_eval_folds(training_file, n_splits=6):
	"""
	Generate {n_splits} fold for the cross-validation

	training_file: A file with training data (sequence)
	n_splits: Number of fold 
	if k=6, there is 5 train folds and 1 eval fold. 5folds=83% and 1fold=17%
	"""

	kf = KFold(n_splits=n_splits, shuffle=True)

	all_training_fold = [] # contains k-training fold
	all_eval_fold = [] # contains k-test fold

	for _, (train_index, eval_index) in enumerate(kf.split(training_file['sequence'])):
		all_training_fold.append(train_index)
		all_eval_fold.append(eval_index)

	return all_training_fold, all_eval_fold


# --- EVALUATION ---


def run_evaluation(population, dbmotif, config, training_file, train_folds, eval_folds):
	"""
	Run the evaluation step on the whole population. 
	The process is parallelized using a pool and the starmap function. Return the population
	with all evaluated individuals. 
	
	population: The population to evaluate
	dbmotif: A dict() with information about motifs detected in the training set
	config: The dictionnary with configuration parameters
	training_file: A file with training data (sequence)
	train_folds: Contain a set of lists, each list containing the indexes of the training sequences 
	eval_folds: Contain a set of lists, each list containing the indexes of the evaluation sequences 

	return: evaluated_population, individuals with adjusted scores
	"""

	# create list of pools
	# try:
	pool = multiprocessing.Pool(int(config['cpu']))
	# except KeyError:
	# 	nb_cpu = multiprocessing.cpu_count()
	# 	pool = multiprocessing.Pool(nb_cpu) 

	m = multiprocessing.Manager()
	evaluated_pop  = m.list() # output list managed by the manager process m
	
	# Evaluation step, parallelized with starmap
	evaluated_population = pool.starmap(evaluate_individual, [(indi, evaluated_pop, dbmotif, training_file, train_folds, eval_folds, config) for indi in population])

	pool.close()
	pool.join()

	return evaluated_population

def run_random_evaluation(population, config):
	"""
	Run the evaluation step on the whole population without the weight adjustment. 
	The process is parallelized using a pool and the starmap function. Return the population
	with all evaluated individuals. 
	
	population: The population to evaluate
	config: The dictionnary with configuration parameters

	return: evaluated_population, individuals with adjusted scores
	"""

	pool = multiprocessing.Pool() # create list of pools
	m = multiprocessing.Manager()
	evaluated_pop  = m.list() # output list managed by the manager process m

	l_seq = []
	l_cest = []
	with open(f'data/{config["filename"]}/train.csv', 'r') as file_R1:
		next(file_R1)

		for line in file_R1:
			line = line.strip().split(',')
			sequence = line[0]
			cest_value = float(line[1])
			l_seq.append(sequence)
			l_cest.append(cest_value)
	
	# Evaluation step, parallelized with starmap
	evaluated_population = pool.starmap(evaluate_individual_random, [(indi, evaluated_pop, l_seq, l_cest, config) for indi in population])

	pool.close()
	pool.join()

	return evaluated_population

def evaluate_individual_random(individual, evaluated_pop, l_seq, l_value, config):
	"""
	Evaluate an individual and define his fitness value. Return the evaluated individual
	No kfolding

	individual: The individual to evaluate
	l_seq: All training sequences
	l_value: All values associated to the training sequences
	evaluated_pop: List of new evaluated individual
	config: The dictionnary with configuration parameters

	return: indidividual
	"""		
	
	# r_eval  = evaluation_on_dataset_random(individual, l_seq, l_value, config)
	r_train = evaluation_on_dataset_random(individual, l_seq, l_value, config)
	
	individual.fitness = r_train
	individual.train_score = r_train

	evaluated_pop.append(individual)

	return individual

def evaluate_individual(individual, evaluated_pop, dbmotif, training_file, train_folds, eval_folds,config):
	"""
	Evaluate an individual and define his fitness value. Return the evaluated individual

	individual: The individual to evaluate
	evaluated_pop: List of new evaluated individual
	dbmotif: A dict() with information about motifs detected in the training set
	training_file: A file with training data (sequence)
	train_folds: Contain a set of lists, each list containing the indexes of the training sequences 
	eval_folds: Contain a set of lists, each list containing the indexes of the evaluation sequences 
	config: The dictionnary with configuration parameters

	return: indidividual
	"""	

	individual.reset()
	# Logistic function to determine a weight according to the number of regex, to control the number of rules
	wgt = np.exp(0.1 * - len(individual.rules)) # not currently in use

	# K-fold training
	individual.fitness, individual.train_score = kfolding(individual,
														  wgt,
														  dbmotif,
														  training_file,
														  train_folds,
														  eval_folds,
														  config)

	evaluated_pop.append(individual)

	return individual

def kfolding(individual, wgt, dbmotif, training_file, train_folds, eval_folds, config):
	"""
	Cross validation with K-fold method to avoid overfitting. Train on k-1 fold and evaluate on the last fold.
	Repeat the step and calculate and return the r mean for each eval-fold. 

	individual: The individual to evaluate
	wgt: a weight to adjust the number of regex
	dbmotif: A dict() with information about motifs detected in the training set
	training_file: A file with training data (sequence)
	train_folds: Contain a set of lists, each list containing the indexes of the training sequences 
	eval_folds: Contain a set of lists, each list containing the indexes of the evaluation sequences 
	config: The dictionnary with configuration parameters

	return: score
	"""

	train_scores = []
	eval_scores = []
	min_motif_size = int(config['min_motif_size_db'])
	max_motif_size = int(config['max_motif_size_db'])
	threshold = float(config['threshold'])

	for k, fold_train in enumerate(train_folds):
		individual.reset() # reset score, match et db_motif
		dict_regex_score = {}

		for index in fold_train:
			# Train step by adjusting score according to motifs in the training set
			training(individual, dict_regex_score, training_file['sequence'], min_motif_size, max_motif_size, dbmotif, index, threshold, wgt)

		# Evaluation on the k-ieme fold
		r_eval  = evaluation_on_dataset(eval_folds[k], training_file['sequence'], training_file['fitness'], dict_regex_score, config)
		r_train = evaluation_on_dataset(train_folds[k], training_file['sequence'], training_file['fitness'], dict_regex_score, config)

		eval_scores.append(r_eval)
		train_scores.append(r_train)

	score_eval = np.mean(eval_scores)
	score_train = np.mean(train_scores)

	return score_train, score_eval

def training(individual, dict_regex_score, training_file, m_min, m_max, dbmotif, index, threshold, wgt, penalty=5):
	'''
	The training step where the score of the individual are calculated according to their matches
	in each sequence of the training file

	individual: The individual to evaluate
	dict_regex_score: final dict with the RE pattern and the final score
	training_file: A file with training data (sequence)
	m_min: minimal size of motifs
	m_max: maximal size of motifs
	dbmotif: A dict() with information about motifs detected in the training set
	index: number of the sequence in the training set
	threshold: threshold to determine if a motif is beneficial or not
	wgt: a weight to adjust the number of regex
	penalty: to reduce the value of uninteresting motifs
	'''

	global MAX


	if MAX > 10:
		divide = 100
	else:
		divide = 1

	for rule in individual.rules:
		regex_rule = re.compile(rule.pattern)

		# Search match in 'sequence'. Non-overlapping matches of the expression    
		for match in re.finditer(regex_rule, training_file[index]):
			motif = match.group()
			len_motif = len(motif)

			if m_min <= len_motif <= m_max:
				rule.nb_match += 1

				if motif in dbmotif:
					# wgt_occ = dbmotif[motif][0] / MAX
					weight = dbmotif[motif][1]

					y = (weight * len_motif) # *wgt

					if weight >= threshold:  
						rule.score += y/divide
					else: 
						# rule.score -= (y * penalty) /100
						rule.score -= y/divide
					
					dict_regex_score[rule.pattern] = rule.score

def evaluation_on_dataset(eval_fold, seq_file, value_file, dico, config):
	"""
	Evaluation of each regex on the sequences of the evaluation fold. 
	Returns the correlation coefficient r.

	eval_fold: A list containing the indexes of the evaluation fold
	seq_file: All training sequences
	value_file: All values associated to the training sequences
	dico: Dict with the regex/rules and the his adjusted score
	config: The dictionnary with configuration parameters

	return r
	"""

	data_dico = {'value': [], 'Predicted_score': []}
	min_motif_size = int(config['min_motif_size_db'])

	for index in eval_fold:
		predicted_score = 0
		sequence = seq_file[index]
		value = value_file[index]

		for regex, score in dico.items():
			if score != 0:
				regex = re.compile(regex)
	
				for match in re.finditer(regex, sequence):

					if len(match.group()) >= min_motif_size:
						predicted_score += score

		data_dico['Predicted_score'].append(predicted_score)
		data_dico['value'].append(value)


	df = pd.DataFrame.from_dict(data_dico)

	with warnings.catch_warnings(): # avoid displaying a warning message 
		warnings.filterwarnings("ignore", category=stats.ConstantInputWarning)
		try:
			r, p_val = sp.stats.pearsonr(df['value'], df['Predicted_score'])
		except ValueError: # if small dataset and not datapoint enough (<2)
			r, p_val = 0, 0

	if math.isnan(r):
		return 0
	else:
		return r

def evaluation_on_dataset_random(individual, l_seq, l_value, config):
	"""
	Evaluation of each regex on the sequences of the evaluation fold. 
	Returns the correlation coefficient r.

	l_seq: All training sequences
	l_value: All values associated to the training sequences
	config: The dictionnary with configuration parameters

	return r
	"""

	data_dico = {'value': [], 'Predicted_score': []}
	min_motif_size = int(config['min_motif_size_db'])
	
	for sequence, VALUE in zip(l_seq, l_value):
		predicted_score = 0

		for rule in individual.rules:
			score = int(rule.score)
			regex = re.compile(rule.pattern)
	
			for match in re.finditer(regex, sequence):
				if len(match.group()) >= min_motif_size:
					rule.nb_match+=1
					predicted_score += score
	
		data_dico['Predicted_score'].append(predicted_score)
		data_dico['value'].append(VALUE)

	df = pd.DataFrame.from_dict(data_dico)
	
	with warnings.catch_warnings(): # avoid displaying a warning message 
		warnings.filterwarnings("ignore", category=stats.ConstantInputWarning)
		try:
			r, p_val = sp.stats.pearsonr(df['value'], df['Predicted_score'])
		except ValueError: # if small dataset and not datapoint enough (<2)
			r, p_val = 0, 0
	
	if math.isnan(r):
		return 0
	else:
		return r




# --- CROSSOVER ---

def run_crossover(config, population, newPop):
	"""
	Run the crossover step, selecting all parents with the
	tournament method, then combining two to obtain two new offspring
	
	config: The dictionnary with configuration parameters
	population: Current population
	newPop: New empty population where to add the offspring
	"""

	pressure = int(config['tournament_size'])

	# Selection of the parents with Tournament method
	selected_parents = selection_of_parents(population, pressure)

	for i in range(0, len(selected_parents), 2): # 2 by 2
		parentA = selected_parents[i]
		parentB = selected_parents[i+1]

		if R.random() < float(config['proba_xover']) : # (default=0.9)	
						
			# Create 2 new empty offspring
			offspring1 = I.Individual(config) 
			offspring2 = I.Individual(config) 

			# Crossover operator: obtain 2 new list of rules
			r1, r2 = crossover_dual(config, parentA, parentB)
		
			# Define the rules of the new offspring
			offspring1.rules = r1
			offspring2.rules = r2
		
			# Sort rules according to their score --- utile ???
			offspring1.scoreSort() 
			offspring2.scoreSort() 

			# Resize the offspring so it doesn't exceed the maximum allowed count
			if len(offspring1.rules) > int(config['maximum_rule_count']):
				offspring1.rules = offspring1.rules[:int(config['maximum_rule_count'])]

			if len(offspring2.rules) > int(config['maximum_rule_count']):
				offspring2.rules = offspring2.rules[:int(config['maximum_rule_count'])]
		
			# Insertion of the new offspring in the population
			newPop.append(offspring1)
			newPop.append(offspring2)
		else:
			newPop.append(parentA)
			newPop.append(parentB)

def selection_of_parents(population, pressure):
	"""
	Selecting all parents with tournament method, for the crossover.
	Return the list with X parents where X= len(population)

	population: The initial population containing all evaluated individuals
	pressure: Size of the tournament.

	return: selected_parents
	"""

	selected_parents = []

	for _ in range(len(population)): 
		best = tournament(population, pressure)
		selected_parents.append(best)

	return selected_parents

def tournament(population, selection_pressure):
	'''
	The tournament operator. Select the best individual
	on a subset of size {selection_pressure} 
	
	population: the population to draw individual for the tournament
	selection_pressure: The size of the tournament (default = 5)

	return: best_parent
	'''

	# Random selection of X individual without replacement (Avoid to draw the same individual twice in a tournament)
	tournament = R.sample(population, selection_pressure)

	# sort individual by fitness value
	sorted_indices = list(reversed(sorted(tournament, key=lambda x: x.fitness)))

	return sorted_indices[0]

def crossover_dual(config, parentA, parentB):
	"""
	Crossoverbetween two individuals.
	The list of rules of an individual is cut in 2 groups
	and one of the groups is exchanged with the second individual.
	Return two new lists of rules containing
	the combination of the lists of each parent.

	config: The dictionnary with configuration parameters
	parentA: First selected parent
	parentB: Second selected parent

	return: rules_off1, rules_off2
	"""

	if len(parentA.rules) == 0 or len(parentB.rules) == 0:
		print('\033[91m[ERROR]\033[0m A parent is empty, impossible to realize the crossover')
		if config['verbose']:
			print("PARENT 1")
			parentA.show()
			print("PARENT 2")
			parentB.show()
		exit()
	else:
		# define the limits where to cut the table of rules
		cutA = R.randint(0, len(parentA.rules)-1)
		cutB = R.randint(0, len(parentB.rules)-1)

		rules_off1 = copy.deepcopy(parentA.rules[:cutA]) + copy.deepcopy(parentB.rules[cutB:])
		rules_off2 = copy.deepcopy(parentB.rules[:cutB]) + copy.deepcopy(parentA.rules[cutA:])

		return rules_off1, rules_off2


# --- MUTATIONS ---

def run_mutations(config, indv, dico_cnt_mut, glob_proba=False):
	"""
	Run each mutation with a probability p. 

	config: The dictionnary with configuration parameters
	indv: The individual to mutate
	dico_cnt_mut: dictionnary with the number of mutation per run
	"""

	last_layer, dict_layer = which_layer(config)

	# add a new rule/regex
	if len(indv.rules) < int(config['maximum_rule_count']): 		
		if R.random() <= float(config['mut_add_rule']):
			MUT_add_rule(config, indv)
			dico_cnt_mut['cnt_add_rule'] += 1

	# remove a rule/regex
	if len(indv.rules) > 1: 					
		if R.random() <= float(config['mut_remove_rule']):
			MUT_remove_rule(indv)
			dico_cnt_mut['cnt_remove_rule'] += 1

	# replace a rule/regex
	if len(indv.rules) >= 2: 					
		if R.random() <= float(config['mut_replace_rule']):
			MUT_replace_rule(config, indv)
			dico_cnt_mut['cnt_replace_rule'] += 1

	# Replace a subtree (modify a part/branch of the tree)
	if len(indv.rules) > 1:
		if R.random() <= float(config['mut_replace_subtree']): 				
			MUT_replace_subtree(config, R.choice(indv.rules), last_layer, dict_layer)
			dico_cnt_mut['cnt_replace_subtree'] += 1
	
	# Add X new AA in a leaf node
	if len(indv.rules) > 1:
		if R.random() <= float(config['mut_add_aa']): 					
			MUT_add_alphabet(config, R.choice(indv.rules), last_layer, dict_layer)
			dico_cnt_mut['cnt_add_aa'] += 1

	# Replace the value of one node (invert or replace)
	if len(indv.rules) > 1:
		if R.random() <= float(config['mut_point']): 					
			MUT_replace_node(config, R.choice(indv.rules))
			dico_cnt_mut['cnt_replace_node'] += 1

	# Remove a subtree
	if R.random() <= float(config['mut_remove_from_pattern']):
		if len(indv.rules) > 1:
			MUT_remove_subtree(R.choice(indv.rules), dict_layer, last_layer)
			dico_cnt_mut['cnt_remove_subtree'] += 1

	# change weight
	if config['random']:
		if len(indv.rules) >= 1:
			if R.random() <= 0.2:
				MUT_change_weight(R.choice(indv.rules))

def create_new_individual(config):
	'''
	Generate a new individual according to the strategy

	config: The dictionnary with configuration parameters

	return: pattern of the RE and the RE tree (list)
	'''

	# Create a new RE pattern
	if config['init_pop_met'] == 'full':
		pattern_re, tree = regex_lib.indi_full(int(config['max_depth_tree']), int(config['min_braces']), int(config['max_braces']), config)
	elif config['init_pop_met'] == 'grow':
		pattern_re, tree = regex_lib.indi_grow(int(config['max_depth_tree']), int(config['min_braces']), int(config['max_braces']), config)
	elif config['init_pop_met'] == 'half':
		pattern_re, tree = regex_lib.indi_half(int(config['max_depth_tree']), int(config['min_braces']), int(config['max_braces']), config)

	return pattern_re, tree

def MUT_add_rule(config, individual):
	'''
	Add a new regex to the list of rules of an individual
	
	config: The dictionnary with configuration parameters
	individual: the target individual selected with a probability
	'''

	pattern, tree = create_new_individual(config)
	weight = round(R.uniform(-50,50), 2)

	# create a new rule
	rule = Rule.Rule(pattern, tree, weight)
	individual.rules.append(rule)

def MUT_remove_rule(individual):
	'''
	Remove a rule to the list of rules of an individual

	individual: the target individual selected with a probability p
	'''

	tempRand = R.randint(0, len(individual.rules) - 1)
	del (individual.rules[tempRand])

def MUT_replace_rule(config, individual):
	"""
	Replace a rule by another one.

	config: The dictionnary with configuration parameters
	individual: the target individual selected with a probability
	"""
	MUT_remove_rule(individual)
	MUT_add_rule(config, individual)

def MUT_replace_subtree(config, rule, last_layer, dict_layer):
	"""
	Replace a subtree by another one

	config: The dictionnary with configuration parameters
	rule: The rule/regex impacted by the mutation
	last_layer: Number of the last layer
	dict_layer: Dictionnary with the index of each node of each layer
	"""

	# Draw a random node
	random_node = pick_a_node(rule.tree_shape)
	maxnodes = (2**int(config['max_depth_tree']))-1

	if random_node == None:
		return 0

	# Define the layer of the random node
	for num_layer, list_nodes in dict_layer.items():
		if random_node in list_nodes:
			layer = num_layer

	# Leaf case, node in the last layer
	if layer == last_layer:
		# Change the leaf and modify tree shape and pattern of the rule
		# if the leaf is a list
		if isinstance(rule.tree_shape[random_node], list):
			if '^' in rule.tree_shape[random_node]: # [^] case
				rule.tree_shape[random_node] = R.sample(regex_lib.ALPHABET, R.randint( int(len(regex_lib.ALPHABET)/2), len(regex_lib.ALPHABET)-1))
				rule.tree_shape[random_node].insert(0,'^')
			else: # [] case
				rule.tree_shape[random_node] = R.sample(regex_lib.ALPHABET, R.randint(1, int(len(regex_lib.ALPHABET)/2)+1 ))

		# if the leaf is a single element
		else:
			rule.tree_shape[random_node] = R.choice(regex_lib.LAST)

	# Operator node case
	else:
		# Generate a new sub-tree according to a initialization method
		tree_method = np.random.choice(['full', 'grow'], p=[0.5,0.5])

		if tree_method == 'full':
			new_regex, new_tree = regex_lib.indi_full( (int(config['max_depth_tree']) - layer)+1, int(config['min_braces']), int(config['max_braces']), config) 
		elif tree_method == 'grow':
			new_regex, new_tree = regex_lib.indi_grow( (int(config['max_depth_tree']) - layer)+1, int(config['min_braces']), int(config['max_braces']), config) 

		rule.tree_shape[random_node] = new_tree[0]

		# Create list of children of the random node
		child = [random_node] 
		i=0

		# Add all nodes of the new subtree
		while (i*2)+1 != maxnodes: 
			if i in child:
				child.append((i*2)+1) # left
				child.append((i*2)+2) # right
			i+=1

		# Replace nodes
		for j, node_index in enumerate(child):
			try:
				rule.tree_shape[node_index] = new_tree[j]
			except IndexError:
				rule.tree_shape[node_index] = None

	# New regex pattern
	rule.pattern = regex_lib.tree2regex(rule.tree_shape)

def MUT_add_alphabet(config, rule, last_layer, dict_layer):
	"""
	Add 1 to 4 new AA in a leaf node. Ex:
	Initial: (H{4}|(P+))|(O+)
	Tree shape: ['|', '|', '+', '{4}', '+', 'O', None, 'HOHAA', None, 'P', None, None, None, None, None]
	New:(HOHAA{4}|(P+))|(O+)
		  ****

	config: The dictionnary with configuration parameters
	rule: The rule/regex impacted by the mutation
	last_layer: Number of the last layer
	dict_layer: Dictionnary with the index of each node of each layer
	"""

	# Draw a random node
	random_node = pick_a_node(rule.tree_shape)
	maxnodes = (2**int(config['max_depth_tree']))-1

	if random_node == None:
		return 0

	# Define the layer of the random node
	for key_layer, list_nodes in dict_layer.items():
		if random_node in list_nodes:
			layer = key_layer

	if layer == last_layer: # Modify only leaf node (no list)
		if not isinstance(rule.tree_shape[random_node], list):
		# if type(rule.tree_shape[random_node]) != list:
			nbr_new_aa = R.randint(1, 4) # Number of AA to add

			for i in range(nbr_new_aa):
				aa = R.choice(regex_lib.LAST)
				rule.tree_shape[random_node] += aa # Add new AA

			# Aply modification on the pattern of the rule
			rule.pattern = regex_lib.tree2regex(rule.tree_shape)

def MUT_replace_node(config, rule):
	"""
	Replace a specific node.

	config: The dictionnary with configuration parameters
	rule: The rule/regex impacted by the mutation
	"""

	# pick a random node (avoid None node)
	random_node = pick_a_node(rule.tree_shape)
	if random_node == None:
		return 0

	if rule.tree_shape[random_node] in regex_lib.LAST:
		aa = R.choice(regex_lib.LAST)
		rule.tree_shape[random_node] = aa

	if rule.tree_shape[random_node] == 'cat':
		rule.tree_shape[random_node] = '|'

	elif rule.tree_shape[random_node] == '|':
		rule.tree_shape[random_node] = 'cat'
	
	elif '{' in rule.tree_shape[random_node]:
		rule.tree_shape[random_node] = '{' + str(R.randint(int(config['min_braces']), int(config['max_braces']))) + '}'

	elif rule.tree_shape[random_node] == '[]':
		try:
			rule.tree_shape[random_node] = '[^]'
			rule.tree_shape[(random_node*2) + 1].insert(0,'^')
		except:
			pass

	elif rule.tree_shape[random_node] == '[^]':
		rule.tree_shape[random_node] = '[]'
		try:
			if ('^' in rule.tree_shape[(random_node*2) + 1]):			
				rule.tree_shape[(random_node*2) + 1].remove('^')
		except:
			rule.tree_shape[random_node] = None

			for node in range(len(rule.tree_shape)):
				if rule.tree_shape[node] == '|':
					if rule.tree_shape[(node*2) + 1] == None:
						rule.tree_shape[node] = 'cat'
					if rule.tree_shape[(node*2) + 2] == None:
						rule.tree_shape[node] = 'cat'

			rule.pattern = regex_lib.tree2regex(rule.tree_shape)



	elif isinstance(rule.tree_shape[random_node], list):
		parentNode = I_am_ur_parent(random_node)
		if rule.tree_shape[random_node][0] == '^':
			del rule.tree_shape[random_node][0]
			rule.tree_shape[parentNode] = '[]'

		else:
			rule.tree_shape[random_node].insert(0,'^')
			rule.tree_shape[parentNode] = '[^]'

	rule.pattern = regex_lib.tree2regex(rule.tree_shape)

def MUT_remove_subtree(rule, dict_layer, last_layer):
	"""
	Remove a branch in the tree.

	rule: The rule/regex impacted by the mutation
	last_layer: Number of the last layer
	dict_layer: Dictionnary with the index of each node of each layer
	"""

	copysave = copy.deepcopy(rule.tree_shape)
	random_node = pick_a_node(rule.tree_shape) # don't select root and None nodes
	
	if random_node in dict_layer[last_layer]:
		return 0
	elif random_node == None:
		return 0
	else:
		remove_branch(rule.tree_shape, random_node)
		check_or(rule.tree_shape)

		rule.pattern = regex_lib.tree2regex(rule.tree_shape)

		if rule.pattern == None:
			rule.tree_shape = copysave
			rule.pattern = regex_lib.tree2regex(rule.tree_shape)


def MUT_change_weight(rule):
	rule.score = R.randint(-50,50)

def I_am_ur_parent(indexNode):
	"""
	Return the parent node of the target node
	indexNode: Children node

	return: int((indexNode-1)/2) | int((indexNode-2)/2)
	"""

	if indexNode == 0:
		return 0
	else:
		if indexNode%2 == 0:
			return int((indexNode-2)/2)
		else:
			return int((indexNode-1)/2)

def pick_a_node(tree_shape):
	"""'
	Pick a random node (avoid None node). Return a integer corresponding to the index of a random node
	
	tree_shape: the tree of a regex in which a random node is drawn.

	return: a node
	"""

	index = [i for i, val in enumerate(tree_shape) if i != 0 and val is not None]

	if len(index) > 0:
		return R.choice(index)
	else:
		return None

def which_layer(config):
	"""
	Define and return the number of the last layer and the node indexes for each layer

	config: The dictionnary with configuration parameters

	return: last_layer, dict_layer
	"""

	last_layer, node = 0, 0
	dict_layer = {}

	# from last layer to the root (reverse direction)
	for j, layer in enumerate(range( int(config['max_depth_tree'])-1, -1, -1)):

		if j == 0:
			last_layer = layer+1

		# Count all nodes in the layer
		for i in range (2**(int(config['max_depth_tree']) - (layer+1))):
			node+=1

			if (int(config['max_depth_tree'])-layer) not in dict_layer.keys():
				dict_layer[int(config['max_depth_tree'])-layer] = []
				dict_layer[int(config['max_depth_tree'])-layer].append(node-1)
			else:
				dict_layer[int(config['max_depth_tree'])-layer].append(node-1)

	return last_layer, dict_layer

def remove_branch(tree, i):
	'''
	Remove recursively the subtree

	tree: the tree shape
	i: the random node where to cut a subtree
	'''

	if i >= len(tree) or tree[i] is None:
		return

	tree[i] = None
	child_left = (i*2) + 1
	child_right = (i*2) + 2
	
	remove_branch(tree, child_left)
	remove_branch(tree, child_right)

def check_or(tree):
	'''
	Correction of the tree shape if a 'or' (|) node has a None children.

	tree: the tree shape
	'''

	for i, node in enumerate(tree):
		if node == '|':
			if tree[(i*2) + 1] == None:  
				tree[i] = 'cat'
			if tree[(i*2) + 2] == None:
				tree[i] = 'cat'
	


# --- GRAPHICS ---

def final_graphics(config, training_file, dev_fitness_count, dev_rule_count, best_eval_score, best_train_score):
	'''
	Generates results figures. All figures are saved in self.plot_dir
	dev_fitness_count: list with std. dev. of fitness values for each run
	dev_rule_count: list with std. dev. of rules count for each run
	'''
	global loading

	evaluate_instance = Evaluation.Eval(config)
	
	test_set = pd.read_csv(f'data/{config["filename"]}/test.csv', sep=',')

	# df data
	df_training = evaluate_instance.eval_regex_df(training_file)
	df_test = evaluate_instance.eval_regex_df(test_set)
	df_metrics = pd.read_csv(config["output_evo"], sep=';')

	# Parameters
	plt.rcParams.update({'axes.spines.right': False,
						'axes.spines.top': False,
						'font.size': 10,
						'savefig.format': 'png'})

	# create and save the figures separately 
	plot_fitness(config, df_metrics, dev_fitness_count)
	plot_evaluation(config, df_training, df_test)
	plot_time(config, df_metrics)
	plot_rule_count(config, df_metrics, dev_rule_count)
	plot_train(config, best_eval_score, best_train_score )
	
def plot_train(config, best_eval_score, best_train_score, name='Train_plot'):
	plt.plot(range(len(best_eval_score)), best_eval_score, label='eval')
	plt.plot(range(len(best_eval_score)), best_train_score, label='train')

	plt.legend()
	plt.savefig(config["output_fig"] + name + '.png', dpi=300, format='png')

def plot_fitness(config, df, dev_fitness, name='Fitness_plot', colors=['#f39c12', '#27ae60']):
	sns.lineplot(data=df, x='Run', y='best_fitness', color=colors[1], linewidth=2, label='Best')
	sns.lineplot(data=df, x='Run', y='average_fitness', color=colors[0], linewidth=2, label='Avg')

	plt.xlabel('Run')
	plt.ylabel('Fitness')

	plt.legend(loc='lower right')

	plt.tight_layout()
	plt.savefig(config["output_fig"] + name + '.png', dpi=300, format='png')
	plt.close()

def plot_rule_count(config, df, dev_rc, name='Rule_count_plot', colors=['#f39c12','#27ae60']):
	sns.lineplot(data=df, x='Run', y='best_rule_count', color=colors[1], linewidth=2, label='Best')
	sns.lineplot(data=df, x='Run', y='average_rule_count', color=colors[0], linewidth=2, label='Avg')
	plt.ylabel('# of rules')
	plt.xlabel('Generation')
	plt.legend(loc='lower right')
	plt.tight_layout()

	plt.savefig(config["output_fig"] + name + '.png', dpi=300, format='png')
	plt.close()

def plot_evaluation(config, df_training, df_test, name='Evaluation_plot', colors=['#f39c12', '#27ae60']):
	global loading

	fig, ax = plt.subplots()

	sns.regplot(x='value',y='Predicted_score', data=df_training, line_kws={'color':colors[0], 'lw':0.7 }, scatter_kws={'color':colors[0], "s": 5, 'alpha':0.5}, label='Training')
	sns.regplot(x='value',y='Predicted_score', data=df_test,     line_kws={'color':colors[1],  'lw':0.7 }, scatter_kws={'color':colors[1],  "s": 5, 'alpha':0.5}, label='Test')
	plt.ylabel('Score')
	plt.xlabel('CEST value')

	# compute r
	r_training, p_val_training = sp.stats.pearsonr(df_training['value'], df_training['Predicted_score'])
	r_test, p_val_test = sp.stats.pearsonr(df_test['value'], df_test['Predicted_score'])
	
	plt.text(0.025, 1, 'Train: r={:.2f}, p={:.2g}'.format(r_training, p_val_training), ha='left', va='top', transform=ax.transAxes, fontsize=7)
	plt.text(0.025, 0.95, 'Test:  r={:.2f}, p={:.2g}'.format(r_test, p_val_test),ha='left', va='top', transform=ax.transAxes,fontsize=7)
	plt.legend(loc='lower right')

	if p_val_test < 0.05:
		significance_marker_test = '*'
	else:
		significance_marker_test = ''

	if p_val_training < 0.05:
		significance_marker_train = '*'
	else:
		significance_marker_train = ''

	print('\n\n\033[35m[RESULTS]\033[0m (Pearson) Train: r=',round(r_training,3), 'P-value:', round(p_val_training,4), significance_marker_train)
	print('\033[35m[RESULTS]\033[0m (Pearson) Test:  r=', round(r_test, 3), 'P-value:', round(p_val_test, 4), significance_marker_test)
	loading = True

	plt.tight_layout()
	plt.savefig(config["output_fig"] + name + '.png', dpi=300, format='png')
	plt.close()

def plot_time(config, df_metrics, name='Time_plot', colors=['#0984e3','#27ae60','#f39c12', '#be2edd']):
	sns.lineplot(data=df_metrics, x='Run', y='Time', 	  	  color=colors[0], linewidth=2, label='Global')
	sns.lineplot(data=df_metrics, x='Run', y='Eval_time', 	  color=colors[1], linewidth=2, label='Eval.')
	sns.lineplot(data=df_metrics, x='Run', y='Xover_time', 	  color=colors[2], linewidth=2, label='Xover.')
	sns.lineplot(data=df_metrics, x='Run', y='Mutation_time', color=colors[3], linewidth=2, label='Mut.')
	plt.ylabel('Seconds')
	plt.xlabel('Generation')
	plt.legend(ncol=2, loc='lower left')

	plt.tight_layout()
	plt.savefig(config["output_fig"] + name + '.png', dpi=300, format='png')
	plt.close()


# --- EVOLUTION ---

def evolve(config, initial_population):
	"""
	Main function of the evolutionary process

	config: the dictionnary with configuration parameters
	initial_population: the initial population with random individual
	"""

							###########################################
							###                                     ###
							###            CONFIGURATION            ###
							###                                     ###
							###########################################

	global loading


	# For training
	dbmotif = build_motif_database(config) # Contains {key=motif: value=[occurence, value]}
	training_file = load_training_file(config) # create a df with peptide sequences and their values
	size = sys.getsizeof(training_file)

	train_folds, eval_folds = generate_train_eval_folds(training_file)

	# activate the seed
	seed = int(config['seed'])
	R.seed(seed) 
	print('\033[32m[INFO]\033[0m Seed:', seed)

	if config['verbose']:
		print('\033[32m[INFO]\033[0m Size of training set:', size, 'o')

	# Create output directory
	os.makedirs(config["output_fig"], exist_ok=True)

	dev_fitness_count = [] 	# List: std. dev. of fitness values of all individuals /run
	dev_rule_count = []    	# List: std. dev. of number of rule/regex of all individuals /run
	stop_fitness = []		# List: for the stop condition, store best fitness values
	best_eval_score = []    # List: evaluation scores of the best individual
	best_train_score = []   # List: training scores of the best individual

	# Generate evolutionnary log
	evo_log_header = "Run;best_fitness;best_rule_count;average_fitness;" \
				 	 "average_rule_count;Population_size;Time;Eval_time;" \
				 	 "Xover_time;Mutation_time;mAR;mRR;mReR;mCW;mReS;mRFP;mRN;mAA"
	utils.write_evo_log(config["output_evo"], evo_log_header)



							###########################################
							###                                     ###
							###              EVOLUTION              ###
							###                                     ###
							###########################################



	if config['verbose']:
		print('\033[32m[INFO]\033[0m Running the Evolution process', '\033[92m')

	# First Evaluation of the initial population
	start_fst_eval_time = time.time()

	if config['random']:
		initial_population = run_random_evaluation(initial_population, config)
	else:
		initial_population = run_evaluation(initial_population, dbmotif, config, training_file, train_folds, eval_folds )
	
	bestIndividual = copy.deepcopy(max(initial_population, key=lambda x: x.fitness))

	eval_time = round((time.time() - start_fst_eval_time), 2)

	if config['verbose']:
		print(f'\033[32m[INFO]\033[0m The first evaluation time lasted {eval_time}s')

	# To count the number of mutations
	dico_cnt_mut = {'cnt_add_rule':0,
					'cnt_remove_rule':0,
					'cnt_replace_rule':0,
					'cnt_replace_subtree':0,
					'cnt_add_aa':0,
					'cnt_replace_node':0,
					'cnt_remove_subtree':0}

	# Main evolution loops
	for RUN in tqdm(range(int(config['runs']))):
		start_time = time.time()

		# Break the evolution if fitness don't increase after [stop] runs
		if stop_condition(stop_fitness, stop=int(config['stop'])):
			print(f'[WARNING] the evolutionary process was interrupted at run {RUN} ' \
				  f'because the fitness was not increasing anymore')
			break

		avgFitness = [] 	# Average Fitness of all individuals
		avgFitness2 = [] 	# Average Fitness of all individuals
		avgRuleCount = [] 	# Average number of rule/regex

		# We keep a copy of the elite before crossover and mutation
		elite = copy.deepcopy(bestIndividual)
		
								###########################################
								###                                     ###
								###              CROSSOVER              ###
								###                                     ###
								###########################################	

		newPop = [] # temporary new pop with parents + offspring
		start_Xover_time = time.time()
		
		run_crossover(config, initial_population, newPop)

		Xover_time = round(time.time()-start_Xover_time, 2)
		if config['verbose']:
			print(f'\033[32m[INFO]\033[0m The crossover time lasted {Xover_time}s')

		


								###########################################
								###                                     ###
								###              MUTATIONS              ###
								###                                     ###
								###########################################

		start_mut_time = time.time()

		for individual in newPop: 
			run_mutations(config, individual, dico_cnt_mut, glob_proba=False)

			individual.remove_double_rules()  	# Duplicates are removed + the rules too similar
			individual.scoreSort() 				# Sort individual's rules according to their score

		mutation_time = round(time.time()-start_mut_time, 2)

		if config['verbose']:
			print(f'\033[32m[INFO]\033[0m The mutation time lasted {mutation_time}s')



								###########################################
								###                                     ###
								###              EVALUATION             ###
								###                                     ###
								###########################################

		start_eval_time = time.time()

		# evaluation of each individual of the population
		if config['random']:
			tprPop = run_random_evaluation(newPop, config)
		else:
			tprPop = run_evaluation(newPop, dbmotif, config, training_file, train_folds, eval_folds)
	

		eval_time = round((time.time() - start_eval_time), 2)
		if config['verbose']:
			print(f'\033[32m[INFO]\033[0m The evaluation time lasted {eval_time}s')

		# Selection of the new best individual
		bestIndividual = copy.deepcopy(max(tprPop, key=lambda x: x.fitness))

		if bestIndividual.fitness < elite.fitness:
			bestIndividual = copy.deepcopy(elite)

		best_eval_score.append(bestIndividual.fitness)
		best_train_score.append(bestIndividual.train_score)

		# Add Elite to the temporary population
		tprPop.append(elite) # Elitism

		# replace the population by the tprPop containing only the offspring
		if config['insertion_pop'] == 'generational':
			initial_population.clear() # Erase all individual of pop
			initial_population = copy.deepcopy(tprPop)

		# Add offspring in the previous generation to mix parent + offspring
		elif config['insertion_pop'] == 'mix':
			initial_population += tprPop
		else:
			print('\033[91m[ERROR]\033[0m The population insertion method is wrong, '\
				  'choose between generational or mix, in the config.ini file')
			exit()


								###########################################
								###                                     ###
								###             REDUCTION               ###
								###                                     ###
								###########################################


		start_reduc_time = time.time()
		FinalPop = list(reversed(sorted(initial_population, key=lambda x: x.fitness)))
		FinalPop = FinalPop[:int(config['population_size'])] # Selection of k best individuals

		avgFitness = [indi.fitness for indi in FinalPop]
		avgRuleCount = [len(indi.rules) for indi in FinalPop]

		# Erase all individual from initial_population and replace them by individuals from FinalPop
		initial_population.clear() 
		initial_population = FinalPop

		reduc_time = round((time.time() - start_reduc_time), 2)
		if config['verbose']:
			print(f'\033[32m[INFO]\033[0m The reduction time lasted {reduc_time}s')



								###########################################
								###                                     ###
								###               METRICS               ###
								###                                     ###
								###########################################
		
		# Std. Dev. 
		dev_fitness_count.append(np.std(avgFitness))
		dev_rule_count.append(np.std(avgRuleCount))
		
		# Best 
		bestFitness = round(bestIndividual.fitness, 3)
		bestRuleCount = len(bestIndividual.rules)

		# Average global
		avgFitness = round(mean(avgFitness), 3)
		avgRuleCount = int(round(mean(avgRuleCount), 0))

		# Time/Run
		diff_time = time.time() - start_time

		# Display results
		print_string = f"\n\nRun {RUN}: -best {bestFitness} -brc {bestRuleCount} || -avg {avgFitness} -arc {avgRuleCount} "
		print(print_string)

		if config['verbose']:
			print('\n     \033[32m -- Best Individual --\033[0m\n')
			bestIndividual.show()

		# Log the evolution information
		log_string = f"{RUN};{bestFitness};{bestRuleCount};{avgFitness};{avgRuleCount};{len(initial_population)};{diff_time};{eval_time};{Xover_time};{mutation_time}"
		utils.write_evo_log(config["output_evo"], log_string)

		# Adds the best fitness value, to check if it continues to increase	
		stop_fitness.append(bestFitness)

		# Save the best model of the current run
		utils.save_best_model(config["output_model"], bestIndividual)


		# --- end of a RUN --- #

	print(f'[INFO] Best model saved in {config["output_model"]}')
	print('\n\033[32m[INFO]\033[0m The Best individual is:')
	bestIndividual.scoreSort()
	bestIndividual.show()

	# Display graphics
	loading = False
	animation_thread = threading.Thread(target=loading_animation)
	animation_thread.start()

	final_graphics(config, training_file, dev_fitness_count, dev_rule_count, best_eval_score, best_train_score)
	animation_thread.join()

	if config['verbose']:
		print(f'\n\033[32m[INFO]\033[0m End of the session: results are in ./{config["output_model"]}') 


