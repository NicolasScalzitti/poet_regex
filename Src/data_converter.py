#!/usr/bin/env python3

"""
A script to manage the dataset

@author: N. Scalzitti
@date: 12/16/2022
@department: Department of Engineering - Michigan State University
"""

import os
import pandas as pd
from sklearn.model_selection import train_test_split



INPUT = '../data/Training/Classical.csv'


###### ALPHABET DICTIONNARIES ######

classical = {'A': ['A'],'C': ['C'],'D': ['D'],'E': ['E'],'F': ['F'],
		     'G': ['G'],'H': ['H'],'I': ['I'],'K': ['K'],'L': ['L'],
		     'M': ['M'],'N': ['N'],'P': ['P'],'Q': ['Q'],'R': ['R'],
		     'S': ['S'],'T': ['T'],'V': ['V'],'W': ['W'],'Y': ['Y'],
		     '-': ['-']
			}

zappo = {'A':['I', 'L', 'V', 'A', 'M'], 	# alphatic/hydrophobic
	     'O':['F', 'Y', 'W'],				# aromatic 
	     'P':['K', 'R', 'H'], 				# positive 
	     'N':['D', 'E'], 					# negative 
	     'H':['S', 'T', 'N', 'Q'],		    # hydrophilic 
	     'S':['P', 'G'],					# conformationally special
	     'C':['C'],						    # Just Cysteine
	     '-':['-']}

# Lenckowski, 2007, good for alignment
lenckowski = {'A':['A', 'C', 'K', 'P'],
			  'D':['D', 'H', 'M', 'F', 'S'],
			  'G':['G', 'I', 'V'], 
			  'N':['N', 'T', 'W', 'Y'], 
			  'R':['R', 'Q', 'E', 'L'],
			  '-':['-']}

# the alphabet based on the scoring schema from Cannata, 2002
cannata = {   'I':['I', 'M', 'V', 'L', 'F', 'Y'],
			  	   'P':['P', 'G', 'A', 'T', 'S'],
			  	   'H':['H', 'N', 'Q', 'E', 'D', 'R', 'K'], 
			  	   'W':['W'], 
			  	   'C':['C'],
			  	   '-':['-']}

# Liu 2002, (the alphabet based on the residue pair counts for the MJ matrix
liu1 = {	   'I':['I', 'M', 'V', 'L', 'F'],
		  	   'A':['A', 'C', 'W'],
		  	   'Y':['Y', 'Q', 'H', 'P', 'G', 'T', 'S', 'N'], 
		  	   'R':['R', 'K'], 
		  	   'D':['D', 'E'],
		  	   '-':['-']}

# Liu 2002, (the alphabet based on the residue pair counts for the BLOSUM matrix
liu2 = {      'I':['I', 'M', 'V', 'L'],
			  'F':['F', 'W', 'Y'],
			  'P':['P', 'C', 'A', 'S', 'T'], 
			  'N':['N', 'H', 'Q', 'E', 'D', 'R', 'K'], 
			  'G':['G'],
			  '-':['-']}

# Based on hydrophobicity, Hydrophobe, ~neutre, hydrophyle
kyte_doolittle = {'A': ['A', 'M', 'C', 'F', 'L', 'V', 'I'],
				  'P': ['P', 'Y', 'W', 'S', 'T', 'G'],
				  'R': ['R', 'K', 'D', 'E', 'N', 'Q', 'H'],
				  '-': ['-']}

# Murphy, 2000, alphabet based on the correlation coefficient
sub_8 = {'F': ['F', 'W', 'Y'],
         'C': ['C', 'I', 'L', 'M', 'V'],
         'A': ['A', 'G'],
         'S': ['S', 'T'],
         'E': ['E', 'D', 'N', 'Q'],
         'K': ['K', 'R'],
         'P': ['P'],
         'H': ['H'],
         '-': ['-'] }

sub_4 = {'F': ['F', 'W', 'Y'],
         'C': ['C', 'I', 'L', 'M', 'V'],
         'A': ['A', 'G', 'P', 'S', 'T'],
         'D': ['D', 'E', 'H', 'K', 'N', 'Q', 'R'],
         '-': ['-']}

# hydrophobicity
hydro = {'F' :['F', 'I', 'L', 'V', 'W', 'Y'],
 		 'A' :['A', 'C', 'G', 'M', 'P'],
 		 'D' :['D', 'E', 'H', 'N', 'R'],
 		 'K' :['K', 'Q', 'S', 'T'],
 		 '-': ['-']}

# Charge
charge = {'K': ['K', 'R'],
		  'D': ['D', 'E'],
		  'A': ['A', 'C', 'F', 'G', 'H', 'I', 'L', 'M', 'N', 'P', 'Q', 'S', 'T', 'V', 'W', 'Y'],
		  '-': ['-']}


def create_dico(alphabet_name):
	dico = {}

	with open(f'../data/CONVERSION/{alphabet_name}.csv') as f:
		for line in f:
			line = line.strip().split(";")
			value = line[0]
			key = line[1]
		
			if key not in dico.keys():
				dico[key] = []
				dico[key].append(value)
			else:
				dico[key].append(value)

	return dico

def create_converted_file(output_file, dico):
	'''
	Create a new dataset with converted sequence, depending of the translation dictionnary
	'''
	with open(INPUT, 'r') as file_R1:
		with open(output_file, 'w') as file_W1:
			for i, line in enumerate(file_R1):
				if i !=0:

					line = line.strip().split(',')
					seq = line[0]
					score = line[1]
					classe = line[2]

					print('>>>', seq, score)

					converted_seq = ''

					for aa in seq:
						for k,v in dico.items():
							if aa in v:
								converted_seq += k

					print(converted_seq)

					file_W1.write(f'{converted_seq};{score};{classe}\n')
				else:
					file_W1.write("sequence;fitness;Classe\n")



class Data():
	def __init__(self, name_data, alphabet_name='classical'):
		self.alphabet_name = alphabet_name		# Name of the Alphabet (Classical, Zappo...)
		self.alphabet_path = f'../data/CONVERSION/{self.alphabet_name}.csv' # Path of the Alphabet
		self.name_data = name_data			# Name of raw data file
		self.raw_data_path = f'../data/RAW/{self.name_data}.csv'	# path of the raw data
		self.conversion = False				# if true, convert data with self.alphabet_name

	def convert(self, df, dico):
		'''
		Converti les seq via un alphabet precis contenu dans dico
		'''

		for i, row in df.iterrows():
			converted_seq = ''
			seq = row['sequence']

			for aa in seq:
				converted_seq += dico[aa]

			df.loc[i, 'sequence'] = converted_seq
			
			# data['sequence'][i] = sequence_convertie
		return df


	def create_train_test_set(self):
		# Cree le dossier ou tous les fichiers de cet alphabet seront stocke
		os.makedirs(f'../data/{self.name_data}', exist_ok=True)
		# Charge les raw data
		data = pd.read_csv(self.raw_data_path, sep=',')


		# Charge le ficher d'alphabet pour la conversion
		df = pd.read_csv(self.alphabet_path)
		df.columns = ['acide_aminé', 'code']
		# Genere le dico pour la conversion
		dico = df.set_index('acide_aminé')['code'].to_dict()

		# Converti les raw data en fonction d'un alphabet
		if self.alphabet_name != 'classical':
			data = self.convert(data, dico)	
			train_data, test_data = train_test_split(data, test_size=0.2)
			# sauvegarde les donnees convertis
			train_data.to_csv(f'../data/{self.name_data}/train.csv', index=False)
			test_data.to_csv(f'../data/{self.name_data}/test.csv', index=False)

			# data.to_csv(f'data/{self.name_data}/converted.csv', index=False)
		else:
			# Genere un jeu train et un jeu test
			train_data, test_data = train_test_split(data, test_size=0.2)
			train_data.to_csv(f'../data/{self.name_data}/train.csv', index=False)
			test_data.to_csv(f'../data/{self.name_data}/test.csv', index=False)



if __name__ == '__main__':
	# Physiochemical properties from Zappo
	# zappo_dico = create_dico("Zappo")
	# create_converted_file("../data/Training/Zappo.csv", zappo_dico)

	name_data = input('Name of raw data file: ')
	alphabet = input('Name of the alphabet (Empty = classical): ')
	alphabet = alphabet.lower()
	
	if alphabet == '':
		alphabet = 'classical'

	dataObj = Data(name_data, alphabet)
	dataObj.create_train_test_set()
