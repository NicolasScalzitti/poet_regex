#!/usr/bin/env python3

"""
Evaluate a POET-regex model on a train and test sets

@author: N. Scalzitti
@date: 11/01/2022
@department: Department of Engineering - Michigan State University
"""

import re
import csv
import scipy as sp
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt



class Eval:
	def __init__(self, config):
		self.config = config 									# config file
		self.model = config['output_model'] 					# Load a model
		self.min_size_motif = int(config['min_motif_size_db']) 	# the minimum size of a motif detected by a regex
	
	def eval_regex(self, file, avoidZero=False): # ajout de dico
		'''
		Evaluate the model/all regex on a dataset (file). 
		Extracted patterns have a minimum size of (self.min_size_motif)

		file: path of a dataset

		return a df with value according to the predicted score
		'''
		# dictionnary = dico
		dictionnary = self.model_to_dict()

		data = {'value': [], 'Predicted_score': []}

		# Open the training set
		with open(file, 'r') as csv_file:
			file = csv.reader(csv_file, delimiter=',')
			next(file) # pass the header

			for line in file:
				peptide = line[0]
				VALUE = float(line[1])
				predicted_score = 0

				# identify the motif in peptides with regex
				for regex, score in dictionnary.items():
					regex = re.compile(regex)
					object_find = re.finditer(regex, peptide)
					
					for match in object_find:
						motif = match.group()

						if len(motif) >= self.min_size_motif:
							predicted_score += score

				if avoidZero:
					if predicted_score == 0:
						pass
					else:
						data['Predicted_score'].append(predicted_score)
						data['value'].append(VALUE)
				else:
					data['Predicted_score'].append(predicted_score)
					data['value'].append(VALUE)

		return pd.DataFrame.from_dict(data)



	
	def model_to_dict(self):
		'''
		Create a dictionnary based on a model
		key = regex, value= fitness/score

		return the dictionnary
		'''

		dictionnary = {}

		try:
			with open(self.model, 'r') as csv_file:
				file = csv.reader(csv_file, delimiter=',')
				next(file) # pass the header
	
				for line in file:
					regex = line[1]
					weight= float(line[2])
					dictionnary[regex] = weight
		except FileNotFoundError:
			print(f'\033[91m[ERROR]\033[0m The file {self.model} cannot be found')
			exit()

		return dictionnary


	def eval_regex_df(self, df, avoidZero=False): # ajout de dico
		'''
		Evaluate the model/all regex on a dataset (file). 
		Extracted patterns have a minimum size of (self.min_size_motif)

		file: path of a dataset

		return a df with value according to the predicted score
		'''

		# Convert a model to a dictionary -> key=regex ; value=score
		dictionary = self.model_to_dict() 

		data = {'value': [], 'Predicted_score': []}
		peptides = df['sequence']
		VALUE = df['fitness']

		for _value, peptide in zip(VALUE, peptides):
			predicted_score = 0

			# identify the motif in peptides with regex
			for regex, score in dictionary.items():
				regex = re.compile(regex)
				object_find = re.finditer(regex, peptide)
				
				for match in object_find:
					motif = match.group()
	
					if len(motif) >= self.min_size_motif:
						predicted_score += score

			if avoidZero:
				# Cela signifie que le modele n'a pas trouver pour ces donnees.
				if predicted_score == 0:
					pass
				else:
					data['Predicted_score'].append(predicted_score)
					data['value'].append(_value)
			else:
				data['Predicted_score'].append(predicted_score)
				data['value'].append(_value)

		return pd.DataFrame.from_dict(data)

	def plot_evaluation(self, df_train, df_test,
						colors=['#f39c12','#27ae60'],
						save=False,
						name='Evaluation_plot'):
		'''
		Plot the results of the evaluation

		df_train: path of the training set 
		df_test: path of the test set
		colors: list of colors for the graph
		save: if True, save the figure
		name: name of the saved file
		'''

		coefficients = np.polyfit(df_train['value'], df_train['Predicted_score'], 1)
		print(f"y_train = {round(coefficients[0],2)}x + {round(coefficients[1],2)}")

		r_train, p_val_train = sp.stats.pearsonr(df_train['value'], df_train['Predicted_score'])
		r_test, p_val_test = sp.stats.pearsonr(df_test['value'], df_test['Predicted_score'])

		if p_val_test < 0.05:
			significance_marker_test = '*'
		else:
			significance_marker_test = ''

		if p_val_train < 0.05:
			significance_marker_train = '*'
		else:
			significance_marker_train = ''

		print('\n\033[35m[RESULTS]\033[0m (Pearson) Train: r=',round(r_train,3), 'P-value:', round(p_val_train,4), significance_marker_train)
		print('\033[35m[RESULTS]\033[0m (Pearson) Test:  r=', round(r_test, 3), 'P-value:', round(p_val_test, 4), significance_marker_test)
	
		plt.rcParams.update({'axes.spines.right': False,
							'axes.spines.top': False,
							'font.size': 10})

		fig, ax = plt.subplots()
		sns.regplot(x='value',y='Predicted_score',data=df_train, line_kws={'color':colors[0], 'lw':1 }, scatter_kws={'color':colors[0], "s": 15, 'alpha':0.5}, label='Train', ci=100) # ci= None pour supp the confidence interval 
		sns.regplot(x='value',y='Predicted_score',data=df_test, line_kws={'color':colors[1],  'lw':1 }, scatter_kws={'color':colors[1],  "s": 15, 'alpha':0.5}, label='Test', ci=100)
		plt.ylabel('Predicted Score')
		plt.xlabel('CEST Value')

		plt.text(0.025, 1, 'Train: r={:.2f}, p={:.2g}'.format(r_train, p_val_train), ha='left', va='top', transform=ax.transAxes, fontsize=7)
		plt.text(0.025, 0.95, 'Test: r={:.2f}, p={:.2g}'.format(r_test, p_val_test),ha='left', va='top', transform=ax.transAxes,fontsize=7)
		plt.legend(loc='lower right')
	
		plt.tight_layout()

		save = input("\033[32m[INFO]\033[0m Save figure [y/n]? ")

		if save.lower() == 'y':
			name = input("\033[32m[INFO]\033[0m Name of the figure: ")
			plt.savefig('./output/' + name + '.png', dpi=300, format='png')

		plt.show()

		
