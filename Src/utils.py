#!/usr/bin/env python3

"""
Useful fonctions for POET-REGEX

@author: I. Alavy
@author: N. Scalzitti
@date: 09/23/2022
@department: Department of Engineering - Michigan State University
"""

import csv
import pandas as pd
from statistics import mean

ENDC = '\033[0m'
MOTIF_BANK = {}


def create_log_file(file):
    f = open(file, 'w')
    f.close()
    print(f'\033[32m[INFO]\033[0m File {file} created')

def write_evo_log(path, string):
    '''
    Write the evolutionary log

    path: path of the evolutionary log
    string: A line to add in the file
    '''

    file = open(path, 'a')
    file.write(f"{string}\n")
    file.close()

def save_best_model(path, bestIndividual):
    """ 
    Save the best model

    path: output directory where to save the model
    bestIndividual: best individual/model to save
    """

    data = []
    for i, regex in enumerate(bestIndividual.rules):
        data.append([i+1, regex.pattern, round(regex.score,3)])
        
    df = pd.DataFrame(data, columns=['ID','pattern','weight'])
    df.to_csv(path, index=False)

def printcolor(msg, color='\033[0m'):    
    print(color, msg, ENDC)

def read_config_file(path):
    '''
    Reads parameters from a given config file and generate a dictionary

    path: path of the config file
    return a dictionary with configuration
    '''

    config = {}

    try:
        file = open(path, 'r')
    except IOError as e:
        print(f' \033[91m[ERROR]\033[0m The file {path} does not exist')
        exit()

    lines = file.readlines()

    for num, line in enumerate(lines):
        line = line.split("#")[0].strip()

        if len(line) == 0: # empty line, ignore
            continue
        elif line[0] == "#": # comment
            continue
        else:
            tokens = line.split("=", 1) # illegal length

            if len(tokens) < 2:
                print(f' \033[91m[ERROR]\033[0m Illegal format on line {num+1} of {file.name}')
                exit()
            else:
                config[tokens[0].strip()] = tokens[1].strip()

    return config


# --- MOTIFS DATABASE ---


def open_file(train_file):
    """
    Load sequences and values on the training file

    train_file: name of the dataset used for experiments

    return: dico, a dictionary with keys=peptide sequences, values=values
    """
    dico={}
    with open(f"data/{train_file}/train.csv", newline='') as file_R1:
        csv_file = csv.DictReader(file_R1)
        for line in csv_file:
            dico[line['sequence']] = float(line['fitness'])

    return dico

def standard_motif_bank(motif_size, train_file):
    '''
    Create a motif database for the training step
    standard_motif_bank = extract motif with best value
    Generate a Dict() containing motifs and their class for the training step
    
    motif_size: size of extracted motifs
    train_file: Name of training dataset

    return a Dict() with key=motif, value = [occurence, best value]

    '''
    dico = open_file(train_file)

    exclude = ['-', '.', '_',':']

    for seq, value in dico.items():
        for j in range(len(seq)):
            motif = seq[j:j+motif_size] # sliding window
            if len(motif) == motif_size:
                if seq[j] in exclude:
                    pass
                else:
                    if motif not in MOTIF_BANK.keys(): 
                        MOTIF_BANK[motif] = [1, value] # unknow motif
                    else: 
                        MOTIF_BANK[motif][0] += 1      # known motif

                        # change the value if it is better
                        if value > MOTIF_BANK[motif][1]:
                            MOTIF_BANK[motif][1] = value

    return MOTIF_BANK

def average_motif_bank(motif_size, train_file):
    '''
    average_motif_bank = compute the average value
    Calculate the average value

    motif_size: size of extracted motifs
    train_file: Name of the alphabet (ex: Classical, Zappo...)

    return a Dict() with key=motif, value = [occurence, mean]
    '''

    TPR_DICO = {}
    exclude = ['-', '.', '_',':']

    dico = open_file(train_file)

    for seq, value in dico.items():
        for j in range(len(seq)): # create sliding windows
            motif = seq[j:j+motif_size]

            if len(motif) == motif_size:
                if seq[j] in exclude:
                    pass
                else:
                    if motif not in TPR_DICO.keys():  # new motif
                        TPR_DICO[motif] = [1, [value] ]
                    else: # Known motif
                        # Add a new value associated with the motif in the training set
                        TPR_DICO[motif][1].append(value)
                        TPR_DICO[motif][0] += 1

    for k, v in TPR_DICO.items():
        MOTIF_BANK[k] = []
        MOTIF_BANK[k].append(v[0]) # Number of motifs in all dataset
        MOTIF_BANK[k].append(round(mean(v[1]),2)) # compute the mean

    return MOTIF_BANK

def occ_motif_bank(motif_size, train_file, threshold):
    '''
    occ_motif_bank = extract the number of occurence in each class
    Define the classe of the motif. Counting the number of occurence of class 1 and class 0
    if nbr class 1 > nbr class 0, then motif is class 1, otherwise it is class 0.
    if it is the same number, it compute the difference between value and the threshold.
    Ex, the value of a motif class 1 is 28 and the same motif have another value of 3 (class 0)
    we compute :|12.5-3| = 9.5 and |28 - 12.5| = 15.5, then 15.5 > 9.5 so it is class 1

    motif_size: size of extracted motifs (2-6)
    train_file: Name of the alphabet (ex: Classical, Zappo...)
    threshold: a threshold to define which sequences are interesting with good values (good sequences>threshold )

    return a Dict() with key=motif, value = [occurence, value]

    '''
    dico = open_file(train_file)
    exclude = ['-', '.', '_',':']
    TPR_DICO = {}

    for seq, value in dico.items():
        for j in range(len(seq)): # create sliding windows
            motif = seq[j:j+motif_size]

            if len(motif) == motif_size:
                if seq[j] in exclude:
                    pass
                else:

                    if motif not in TPR_DICO.keys():  # new motif
                        # format [occ, value, value2, counter_class0, counter_class1]
                        if value <= threshold: # 0
                            TPR_DICO[motif] = [1, [value],[0], 1, 0]
                        else: # 1
                            TPR_DICO[motif] = [1, [threshold], [value], 0, 1]

                    else:
                        # add 1 to the motif counter
                        if value <= threshold:
                            TPR_DICO[motif][3] += 1 # counter class 0
                            TPR_DICO[motif][0] += 1 # global counter
                            TPR_DICO[motif][1].append(value) 
                        else:
                            TPR_DICO[motif][4] += 1
                            TPR_DICO[motif][0] += 1
                            TPR_DICO[motif][2].append(value)

    for k, v in TPR_DICO.items():

        MOTIF_BANK[k] = [v[0]] # Number of motifs in all dataset

        if TPR_DICO[k][3] > TPR_DICO[k][4]: # class0 > class1
            MOTIF_BANK[k].extend([mean(v[1])])
        elif TPR_DICO[k][4] > TPR_DICO[k][3]: # class1 > class0
            MOTIF_BANK[k].extend([mean(v[2])])

        elif TPR_DICO[k][3] == TPR_DICO[k][4]: # class1 == class0
            if abs(threshold - mean(v[1])) > abs(mean(v[2]) - threshold):
                MOTIF_BANK[k].extend([mean(v[1])])
            else:
                MOTIF_BANK[k].extend([mean(v[2])])

    return MOTIF_BANK


def truncate(file, create=True):
        """ truncates a given file (creates a file if it doesn't exist) """
        try:
            f = open(file)
            f.truncate()
        except IOError:
            if create:
                f = open(file, "w")
                f.close()
                return 1
            return 0
        return 1

def writeLine(file, str, create=True):
    """ appends a line to a file """
    try:
        f = open(file, "a")
        f.write(str)
        f.write("\n")
    except IOError:
        if create:
            print("dd")
            f = open(file, "w+")
            print("ss")
            f.write(str)
            f.write("\n")
            f.close()
            return 1
        return 0
    finally:
        f.close()
    return 1

def write(file, str, create=True):
    """ writes into a file (doesn't end the line)"""
    try:
        f = open(file, "a")
        f.write(str)
    except IOError:
        if create:
            f = open(file, "w+")
            f.write(str)
            f.close()
            return 1
        return 0
    finally:
        f.close()
    return 1

def writeTruncate(file, str, create=True):
    """ truncates a file and writes into it """
    truncate(file, create)
    write(file, str)