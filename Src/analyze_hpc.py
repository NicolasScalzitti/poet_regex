#!/usr/bin/env python3

"""
Analyse of the HPCC experiments

@author: N. Scalzitti
@date: 3/28/2023
@department: Department of Engineering - Michigan State University
"""

import os
import re
import csv
import sys
import utils
import scipy as sp
import pandas as pd
from statistics import mean, stdev



def model_to_dict(directory, model):
	"""
	Convert a model to a dictionary

	directory: path of the experiment
	model: name of the model

	return: the dictionary
	"""
	dictionary = {}

	try:
		with open(directory+f'/Results/models/{model}', 'r') as csv_file:
			file = csv.reader(csv_file, delimiter=',')
			next(file) # pass the header

			for line in file:
				regex = line[1]
				score= float(line[2])
				dictionary[regex] = score

	except FileNotFoundError:
		print(f'\033[91m[ERROR]\033[0m The file {model} cannot be found')
		exit()

	return dictionary

def eval_regex_df(directory, model, df, config): # ajout de dico
	"""
	Evaluate a model on data present in df 

	directory: path of the experiment
	model: name of the model
	df: name of a df set
	config: file containing all parameters

	return: a df with the predicted value and true value associated
	"""

	dictionary = model_to_dict(directory, model)

	data = {'value': [], 'Predicted_score': []}
	peptides = df['sequence']
	_VALUE = df['fitness']

	for value, peptide in zip(_VALUE, peptides):
		predicted_score = 0

		# identify the motif in peptides with regex
		for regex, score in dictionary.items():
			regex = re.compile(regex)
			object_find = re.finditer(regex, peptide)
			
			for match in object_find:
				motif = match.group()

				if len(motif) >= int(config["min_motif_size_db"]):
					predicted_score += score

		data['Predicted_score'].append(predicted_score)
		data['value'].append(value)

	return pd.DataFrame.from_dict(data)

def evaluation(directory, config):
	"""
	# Evaluation of the model on the train and test set

	directory: path of the experiment
	config: file containing all parameters

	return:
		l_r_train, list of r_train of each model
		l_p_train, list of p value_train of each model
		l_r_test, list of r_test of each model
		l_p_test, list of p value_test of each model
		best, best model
		r_best, best r_test value of the best model

	"""

	training_set = pd.read_csv(f'../data/{config["filename"]}/train.csv', sep=',')
	test_set = pd.read_csv(f'../data/{config["filename"]}/test.csv', sep=',')

	l_r_train = []
	l_r_test = []
	l_p_train = []
	l_p_test = []

	list_model = os.listdir(directory+'/Results/models/')

	best = 0
	r_best = 0

	for model in list_model:
		model_path = directory+f'/Results/models/{model}'

		df_train = eval_regex_df(directory, model, training_set, config)
		df_test = eval_regex_df(directory, model, test_set, config)

		r_train, p_val_train = sp.stats.pearsonr(df_train['value'], df_train['Predicted_score'])
		r_test, p_val_test = sp.stats.pearsonr(df_test['value'], df_test['Predicted_score'])
		l_r_train.append(r_train)
		l_r_test .append(r_test)
		l_p_train.append(p_val_train)
		l_p_test.append(p_val_test)

		if r_test > r_best:
			r_best = r_test
			best = model
		
	return l_r_train, l_p_train, l_r_test, l_p_test, best, r_best

def main():
	if len(sys.argv) < 2:
		print("\033[91m[ERROR]\033[0m Please provide the path to a folder as an argument")
		sys.exit(1)

	directory = sys.argv[1]

	config = utils.read_config_file("../config.ini")

	if not os.path.isdir(directory):
		print("\033[91m[ERROR]\033[0m The folder does not exist.")
		sys.exit(1)


	l_r_train, l_p_train, l_r_test, l_p_test, best_model, r_best = evaluation(directory, config)

	print('\n \033[32m[INFO]\033[0m Best model:', best_model, 'r_test=', r_best, '\n')
	print('==== Mean results ====')

	df = pd.DataFrame(columns=["R_train", "P_val_train", "R_test", "P_val_test"])

	df = df.assign(R_train=[mean(l_r_train)])
	df = df.assign(P_val_train=[mean(l_p_train)])
	df = df.assign(R_test=[mean(l_r_test)])
	df = df.assign(P_val_test=[mean(l_p_test)])

	print(df)

if __name__ == '__main__':
	main()